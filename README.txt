
Net2D - Biblioteca para ajustamento de redes geodesicas para a
determinacao de deslocamentos.

 Compilar a biblioteca:
 
 Em linux.
 Instalar o Mono e correr o script compilar
 
 Em windows.
 Na consola do Visual Studio correr o comando que está no ficheiro compilar_windows.txt

 AVISOS
  - As incertezas ou desvios padrão dados às observações são referentes às
  diferenças de observações. No caso de ter-se o desvio padrão de por exemplo
  uma distancia. Antes de enviar para o respetivo objeto deve proceder-se
  à sua multiplicação por sqrt(2)

  - A única exceção é para a incerteza da distancia quando esta é atribuída
  pelas duas constantes a (mm) e b (ppm), em que este internamente faz essa
  multiplicação.
