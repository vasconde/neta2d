//  Prog2.cs
//
//  Copyright 2013, 2014, 2015 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic; // list

using System.IO;
using System.Text.RegularExpressions;

using Mapack;

namespace NetA2D 
{

  public class Prog2
  {
    static public void Main ()
    {
      // ApA2D.Main();

      // pode ser: network ou a2d
      // string input_type = "network";
      //string input_type = "a2d";
      string input_type = "net2d";

      Network n = new Network();
      Obs o = new Obs();
      ConstraintEq ceq = new ConstraintEq();

      string txtOutput = "eli/OUTPUT.txt"; // tem de ser igual ao 3

      List<string> notas = new List<string>();

      switch (input_type)
	{

	case "network":
	  LegacyIO.NetworkInputToData ("data/network/fagilde/fgl_data.dat",
	   			       n, o, ceq);
	  //LegacyIO.NetworkInputToData ("data/network/alqueva/al.dat",
	  //			       n, o);
	  break;

	case "a2d":
	  LegacyIO.A2DInputToData ("data/a2d/alqueva/crdCor_2013.TXT",
	  			   "data/a2d/alqueva/eqcCor_2013.txt",
	  			   "data/a2d/alqueva/dflCor_2013.txt", 
	  			   n, o, ceq);

	  // LegacyIO.A2DInputToData ("data/a2d/alqueva2/crd.txt",
	  // 			   "data/a2d/alqueva2/eqct2002.txt",
	  // 			   "data/a2d/alqueva2/nov2002t2002.txt", 
	  // 			   n, o);

	  // LegacyIO.A2DInputToData ("data/a2d/fronhas/xyz.txt",
	  // 			   "data/a2d/fronhas/eqc.txt",
	  // 			   "data/a2d/fronhas/dfl.txt", 
	  // 			   n, o);
	  
	  break;

	case "net2d":
	  //Net2DIO.InputTxtToData("data2/epoca103.txt", n, o, ceq, notas, true);
	  Net2DIO.InputTxtToData("eli/alto_ceira_combinado.txt", n, o, ceq, notas, true);
	  break;
	}

      if (true)
	{
	  Console.WriteLine("Numero de Spins:");
	  Console.WriteLine(o.NSpinsDAngs);
	  Console.WriteLine("Numero de angulos por spin:");
	  foreach (SpinDAng ss in o.SpinDAngs)
	    {
	      Console.WriteLine(ss.Spin.Count);
	    }
	  Console.WriteLine("Total de angulos: " + o.NDAngs);
	  Console.WriteLine("Numero de distancias:" + o.NDDists);

	  Console.WriteLine("angulos:");
	  foreach (SpinDAng ss in o.SpinDAngs)
	    {
	      Console.WriteLine("****** lol");
	      foreach(DAng dd in ss.Spin)
		{
		  Console.WriteLine(dd.Sighted.Name);
		}
	    }

	}

      if (true)
	{
	  //equacoes de condicao

	  Console.WriteLine("PONTOS");
	  foreach(Point c in n.Points)
	    {
	      Console.WriteLine("{0,-10}", c.Name);
	    }
	  Console.WriteLine("FIXOS");
	  foreach(DispKnown dk in ceq.DispKnowns)
	    {
	      if(dk.Componente == 0) // ou seja X
		{
		  Console.WriteLine(dk.P.Name + "  " + "X"  + "    " + dk.Residual);
		}
	      if(dk.Componente == 1) // ou seja Y
		{
		  Console.WriteLine(dk.P.Name + "  " + "Y"  + "    " + dk.Residual);
		}

	    }
	}
      
      LSMatrices mats;
      if (true)
	{
	  
	  // comeca o ajustamento

	  Console.WriteLine("********** AJUSTAMENTO *************");

	  mats = new LSMatrices(n, o, ceq);

	  LSAjust aj = new LSAjust(mats);

	  Console.WriteLine();
	  Console.WriteLine(" * ELIPSES *");
	  Console.WriteLine();

	  ErrorEllipse.Apriori (5.99, n, mats.A, mats.W);

	  foreach(Point ppp in n.Points)
	    {
	      if(ppp.HaveErrorEllip)
		{
		  Console.WriteLine(ppp.ErrorEllipParam[1]
				    +
				    "   "
				    + ppp.ErrorEllipParam[2]
				    +
				    "   "
				    + ppp.ErrorEllipParam[3]);
		}
	    }
	  

	  Console.WriteLine();
	  Console.WriteLine(" * ELIPSES *");
	  Console.WriteLine();

	  aj.ComputeAjust();

	  mats.UpdateData ();

	  Console.WriteLine("Resultados: ");
	  foreach(Point p in n.Points)
	    {
	      Console.WriteLine("{0,-10} {1,10:F1} {2,10:F1}", p.Name, p.DX, p.DY);
	    }

	  // o primeiro passo eh verificar se existe pelo menos 1 ponto
	  // cujas coordenadas sao para projetar
	  bool proj =  false;

	  foreach(Point p in n.Points)
	    {
	      if(p.ThetaThere)
		proj = true;
	    }

	  if (proj)
	    {
	      Console.WriteLine();
	      foreach(Point p in n.Points)
		{
		  Console.WriteLine("{0,-10} {1,10:F1} {2,10:F1} {3,10:F1}", p.Name, p.DT, p.DR, p.Theta);
		}
	    }


	  Console.WriteLine("Residuos: ");
	  foreach(SpinDAng sss in o.SpinDAngs)
	    {
	      Console.WriteLine("*");
	      foreach(DAng dang in sss.Spin)
		{
		  Console.WriteLine("{0,-8}{1,-8}{2,-8} {3,10:F1} {4,10:F3}",
				    dang.Origin.Name, dang.Occupied.Name,
				    dang.Sighted.Name, dang.Residual,
				    dang.StdObsA);
		}
	    }
      
	  Console.WriteLine("---");
	  // distancias
	  foreach(DDist ddist in o.DDists)
	    {
	      Console.WriteLine(ddist.Occupied.Name
				+ " " + ddist.Sighted.Name + "    " 
				+ ddist.Residual + "    " + ddist.StdObsA);
	    }

	  Console.WriteLine("--- LOG ---");
	  // distancias
	  foreach(DLnDist dlndist in o.DLnDists)
	    {
	      Console.WriteLine(dlndist.Occupied.Name
				+ " " + dlndist.Sighted.Name + "    " 
				+ dlndist.Residual*dlndist.AproxDist() + "    " + dlndist.StdObsA*dlndist.AproxDist());
	    }

	  Console.WriteLine("--- QUO ---");
	  foreach(SpinDQuo sss in o.SpinDQuos)
	    {
	      Console.WriteLine("*");
	      foreach(DQuo dquo in sss.Spin)
		{
		  Console.WriteLine("{0,-8}{1,-8}{2,-8} {3,20:F10} {4,10:F3} {5,10:F3}",
				    dquo.Origin.Name, dquo.Occupied.Name,
				    dquo.Sighted.Name, dquo.ObsDLnQuo,
				    dquo.StdObsA, dquo.Residual);
		}
	    }

	  // equacoes de condicao
	  foreach(DispKnown dk in ceq.DispKnowns)
	    {
	      
	      if(dk.Componente == 0) // ou seja X
		{
		  Console.WriteLine(dk.P.Name + "  " + "X"  + "    " + dk.Residual);
		}
	      if(dk.Componente == 1) // ou seja Y
		{
		  Console.WriteLine(dk.P.Name + "  " + "Y"  + "    " + dk.Residual);
		}
	    }

	  Console.WriteLine("S0: {0,10:F2}", o.S0);
	}

      // apresentacao de resultados na consola
      OutputTxt outTxt = new OutputTxt (txtOutput, n, o, ceq, notas, true,
					true, true);
      outTxt.WriteResults();

      Console.WriteLine("*********************** AUA ***********");

      foreach(SumPair sp in ceq.SumPairs)
	{
	  Console.WriteLine("{0} {1} {2} | {3} {4} {5} | {6}",
			    sp.SignalFirst, sp.CompFirst, sp.PointFirst.Name,
			    sp.SignalSecond, sp.CompSecond, sp.PointSecond.Name,
			    sp.StdDev);
	}

//      Console.WriteLine(mats.A);
//      Console.WriteLine();
//      Console.WriteLine();
//      Console.WriteLine(mats.W);
    }

  }
}