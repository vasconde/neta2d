//  Ap.cs
//
//  Copyright 2013 Vasco Conde.
//
//  This file is part of Net2D.
//
//  Net2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  Net2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with Net2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;
using System.Collections.Generic; // list

namespace A2D
{

  public class ApA2D
  {
    static public void Main ()
    {
      Network ps = new Network();
      ConstraintEq ceq = new ConstraintEq();
      Obs os = new Obs();
      

      // ADICIONAR OS PONTOS AH REDE
      ps.AddPoint("P1", 39144.30, 337681.00);
      ps.AddPoint("P2", 31299.60, 429909.00);
      ps.AddPoint("P3", 151379.00, 420186.50);
      ps.AddPoint("P5", 165347.60, 340393.90);

      // ADICIONA AS EQUACOES DE CONDICAO
      ceq.AddDispKnown (new DispKnown(ps.PointByName("P1"), 0, 0.0, 0.1));
      ceq.AddDispKnown (new DispKnown(ps.PointByName("P1"), 1, 0.0, 0.1));
      ceq.AddDispKnown (new DispKnown(ps.PointByName("P2"), 0, 0.0, 0.1));
      ceq.AddDispKnown (new DispKnown(ps.PointByName("P2"), 1, 0.0, 0.1));

      // ADICIONA OBSERVACOES GEODESICAS

      // distancias

      double mm = 0.7071067812;
      double ppm = 0;

      os.AddDDist (new DDist(ps.PointByName("P1"),
			     ps.PointByName("P2"),
			     -0.36, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P1"),
			     ps.PointByName("P3"),
			     0.11, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P1"),
			     ps.PointByName("P5"),
			     -5.25, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P3"),
			     ps.PointByName("P1"),
			     1.04, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P3"),
			     ps.PointByName("P2"),
			     0.02, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P3"),
			     ps.PointByName("P5"),
			     2.08, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P5"),
			     ps.PointByName("P1"),
			     -3.90, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P5"),
			     ps.PointByName("P2"),
			     -3.27, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P5"),
			     ps.PointByName("P3"),
			     1.67, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P2"),
			     ps.PointByName("P1"),
			     0.07, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P2"),
			     ps.PointByName("P3"),
			     -0.24, mm, ppm));

      os.AddDDist (new DDist(ps.PointByName("P2"),
			     ps.PointByName("P5"),
			     -3.73, mm, ppm));

      // angulos
      double desvio = Math.Sqrt(2)*0.707;

      //#1

      SpinDAng s = new SpinDAng(ps.PointByName("P5"), 
				ps.PointByName("P1"),
				desvio);

      s.AddDAng (new DAng(ps.PointByName("P5"),
			  ps.PointByName("P1"),
			  ps.PointByName("P2"),
			  -4.50));

      s.AddDAng (new DAng(ps.PointByName("P5"),
			  ps.PointByName("P1"),
			  ps.PointByName("P3"),
			  -10.50));
      os.AddSpinDAng(s);

      //#2
      SpinDAng s1 = new SpinDAng(ps.PointByName("P3"), 
				ps.PointByName("P2"),
				desvio);

      s1.AddDAng (new DAng(ps.PointByName("P3"),
			  ps.PointByName("P2"),
			  ps.PointByName("P5"),
			  21.00));

      s1.AddDAng (new DAng(ps.PointByName("P3"),
			  ps.PointByName("P2"),
			  ps.PointByName("P1"),
			  7.30));
      os.AddSpinDAng(s1);

      //#3
      SpinDAng s2 = new SpinDAng(ps.PointByName("P2"), 
				ps.PointByName("P3"),
				desvio);

      s2.AddDAng (new DAng(ps.PointByName("P2"),
			  ps.PointByName("P3"),
			  ps.PointByName("P5"),
			  36.00));

      s2.AddDAng (new DAng(ps.PointByName("P2"),
			  ps.PointByName("P3"),
			  ps.PointByName("P1"),
			  1.70));
      os.AddSpinDAng(s2);

      //#4
      SpinDAng s3 = new SpinDAng(ps.PointByName("P1"), 
				ps.PointByName("P5"),
				desvio);

      s3.AddDAng (new DAng(ps.PointByName("P1"),
			   ps.PointByName("P5"),
			   ps.PointByName("P2"),
			   13.50));

      s3.AddDAng (new DAng(ps.PointByName("P1"),
			   ps.PointByName("P5"),
			   ps.PointByName("P3"),
			   48.70));
      os.AddSpinDAng(s3);

      SumPair sp = new SumPair(ps.PointByName("P1"),
			       0,
			       0,
			       ps.PointByName("P2"),
			       0,
			       0,
			       Math.Sqrt(2)*0.1);
      ceq.AddSumPair (sp);

      // GERAR MATRIZES

      LSMatrices mats = new LSMatrices(ps, os, ceq);

      // AJUSTAMENTO

      LSAjust aj = new LSAjust(mats);

      aj.ComputeAjust();

      mats.UpdateData ();



      // SAIDA de RESULTADOS


      Console.WriteLine("Pontos");
      foreach(Point p in ps.Points)
	{
	  Console.WriteLine(p.Name);
	}
      Console.WriteLine("Eq. condicao");
      foreach(DispKnown ddd in ceq.DispKnowns)
	{
	  Console.WriteLine(ddd.P.Name);
	}

      Console.WriteLine("Numero de pontos:");
      Console.WriteLine(ps.NPoints);
      Console.WriteLine("Numero de Spins:");
      Console.WriteLine(os.NSpinsDAngs);
      Console.WriteLine("Numero de angulos por spin:");
      foreach (SpinDAng ss in os.SpinDAngs)
	{
	  Console.WriteLine(ss.Spin.Count);
	}
      Console.WriteLine("Total de angulos: " + os.NDAngs);
      Console.WriteLine("Numero de distancias: " + os.NDDists);

      // ---

      Console.WriteLine("Resultados: ");
      foreach(Point p in ps.Points)
      	{
      	  Console.WriteLine(p.Name + "    " + p.DX + "     " + p.DY);
      	}

      Console.WriteLine("Residuos: ");
      foreach(SpinDAng sss in os.SpinDAngs)
      	{
      	  foreach(DAng dang in sss.Spin)
      	    {
      	      Console.WriteLine(dang.Origin.Name + " " + dang.Occupied.Name
      				+ " " + dang.Sighted.Name + "    " 
      				+ dang.Residual);
      	    }
      	}
      
      // distancias
      foreach(DDist ddist in os.DDists)
      	{
      	  Console.WriteLine(ddist.Occupied.Name
      			    + " " + ddist.Sighted.Name + "    " 
      			    + ddist.Residual);
      	}

      // equacoes de condicao
       foreach(DispKnown dk in ceq.DispKnowns)
      	{

	  if(dk.Componente == 0) // ou seja X
	    {
	      Console.WriteLine(dk.P.Name + "  " + "X"  + "    " + dk.Residual);
	    }
	  if(dk.Componente == 1) // ou seja Y
	    {
	      Console.WriteLine(dk.P.Name + "  " + "Y"  + "    " + dk.Residual);
	    }
      	}

       foreach(SumPair spp in ceq.SumPairs)
	 {
	   Console.WriteLine(spp.PointFirst.Name + "  " + spp.PointSecond.Name  + "    " + spp.Residual);
	 }

      Console.WriteLine("S0: ");
      Console.WriteLine(os.S0);

      Console.WriteLine("* * * *");
      Console.WriteLine("{0}", ps.DxtNDx);

    }
  }
}