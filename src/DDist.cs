//  DDist.cs - Observcoes do tipo diferencas de distancias
//
//  Copyright 2013, 2014 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;


namespace NetA2D
{

  public class DDist
  {

    Point occupied;
    Point sighted;

    double obs;

    double stdObsA; // standard deviation apriori

    double residual;

    // GET AND SET
    
    public Point Occupied
    {
      get { return occupied; }
      set { occupied = value; }
    }

    public Point Sighted
    {
      get { return sighted; }
      set { sighted = value; }
    }

    public double Obs
    {
      get { return obs; }
      set { obs = value; }
    }

    public double StdObsA
    {
      get { return stdObsA; }
      set { stdObsA = value; }
    }

    public double Residual
    {
      get { return residual; }
      set { residual = value; }
    }

    // CONSTRUTORES

    public DDist (Point occupied, Point sighted, double obs, double stdObsA,
		  double Ppm, bool isDif)
    {
      this.occupied = occupied;
      this.sighted = sighted;
      this.obs = obs;
      
      // isDif - se eh uma observacao de diferencas de coordenadas
      StdObsAFromAB (stdObsA, Ppm, isDif);
    }

    // public DDist (Point occupied, Point sighted, double obs,
    // 		  double Mm, double Ppm)
    // {
    //   this.occupied = occupied;
    //   this.sighted = sighted;
    //   this.obs = obs;
    //   StdObsAFromAB (Mm, Ppm, false);
    // }

    // FUNCOES

    public double DifX ()
    {
      return sighted.X - occupied.X;
    }

    public double DifY ()
    {
      return sighted.Y - occupied.Y;
    }

    // Retorna a distancia aproximada calculada a partir
    // das coordenadas aproximadas
    public double AproxDist()
    {
      return Math.Sqrt( Math.Pow(DifX(),2) + Math.Pow(DifY(),2) );
    }


    // derivadas em ordem as coordenadas
    // o retorno eh um vetor [d_dxa, d_dya, d_dxb, d_dyb]
    public double[] Derivatives ()
    {
      double[] derivatives = new double[4];

      derivatives[0] = -DifX () / AproxDist(); // d_dxa
      derivatives[1] = -DifY () / AproxDist(); // d_dya
      derivatives[2] = DifX ()  / AproxDist(); // d_dxb
      derivatives[3] = DifY ()  / AproxDist(); // d_dyb
      
      return derivatives;
    }

    // erro baseado nos parametros a e b
    // ATENCAO: estes parametros sao para uma unica distancias
    // o que torna necessario multiplicar por sqrt(2) porque trata-se
    // da incerteza de uma diferenca de observacoes (distancias)
    public void StdObsAFromAB (double Mm, double Ppm, bool isDif)
    {
      if (isDif)
	stdObsA = Mm + AproxDist() * Ppm * 1e-6;
      else
	stdObsA = Math.Sqrt(2.0) * (Mm + AproxDist() * Ppm * 1e-6);
    }

  }

}