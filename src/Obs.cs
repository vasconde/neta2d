//  Obs.cs - Observacoes geodesicas (diferencas de angulos e distanias)
//
//  Copyright 2013, 2015 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;
using System.Collections.Generic; // list

namespace NetA2D
{

  public class Obs
  {

    List <SpinDAng> spinDAngs; // lista de giros de angulos
    List <DDist> dDists;       // lista de distancias
    List <DLnDist> dLnDists;       // lista de ln distancias
    List <SpinDQuo> spinDQuos; // lista de giros de quocientes

    // estatisticas dos residuos

    // distancias e angulos
    double vtWv;
    double df;
    double s0;

    // angular
    double vtWvDAng;
    double dfDAng;
    double s0DAng;

    // linear
    double vtWvDDist;
    double dfDDist;
    double s0DDist;

    // ln linear
    double vtWvDLnDist;
    double dfDLnDist;
    double s0DLnDist;

    // Quo
    double vtWvDQuo;
    double dfDQuo;
    double s0DQuo;

    // soh observacoes
    double vtWvObs;
    double dfObs;
    double s0Obs;

    // construtor
    public Obs()
    {
      spinDAngs = new List <SpinDAng>();
      dDists = new List <DDist>();
      dLnDists = new List <DLnDist>();
      SpinDQuos = new List <SpinDQuo>();
    }


    // GET and SET

    public List<SpinDAng> SpinDAngs
    {
      get { return spinDAngs; }
      set { spinDAngs = value; }
    }

    public List<DDist> DDists
    {
      get { return dDists; }
      set { dDists = value; }
    }

    public List<DLnDist> DLnDists
    {
      get { return dLnDists; }
      set { dLnDists = value; }
    }

    public List<SpinDQuo> SpinDQuos
    {
      get { return spinDQuos; }
      set { spinDQuos = value; }
    }

    // retorna o numero de giros angulos
    public int NSpinsDAngs
    {
      get { return spinDAngs.Count; }
    }

    // retorna o numero de giros quocientes
    public int NSpinsDQuos
    {
      get { return spinDQuos.Count; }
    }


    // global
    public double VtWv
    {
      get { return vtWv; }
      set { vtWv = value; }
    }

    public double Df
    {
      get { return df; }
      set { df = value; }
    }

    public double S0
    {
      get { return s0; }
      set { s0 = value; }
    }


    // angular
    public double VtWvDAng
    {
      get { return vtWvDAng; }
      set { vtWvDAng = value; }
    }

    public double DfDAng
    {
      get { return dfDAng; }
      set { dfDAng = value; }
    }

    public double S0DAng
    {
      get { return s0DAng; }
      set { s0DAng = value; }
    }

    // linear
    public double VtWvDDist
    {
      get { return vtWvDDist; }
      set { vtWvDDist = value; }
    }

    public double DfDDist
    {
      get { return dfDDist; }
      set { dfDDist = value; }
    }

    public double S0DDist
    {
      get { return s0DDist; }
      set { s0DDist = value; }
    }

    public double VtWvDLnDist
    {
      get { return vtWvDLnDist; }
      set { vtWvDLnDist = value; }
    }

    public double DfDLnDist
    {
      get { return dfDLnDist; }
      set { dfDLnDist = value; }
    }

    public double S0DLnDist
    {
      get { return s0DLnDist; }
      set { s0DLnDist = value; }
    }

    public double VtWvDQuo
    {
      get { return vtWvDQuo; }
      set { vtWvDQuo = value; }
    }

    public double DfDQuo
    {
      get { return dfDQuo; }
      set { dfDQuo = value; }
    }

    public double S0DQuo
    {
      get { return s0DQuo; }
      set { s0DQuo = value; }
    }

    //  soh observacoes
    public double VtWvObs
    {
      get { return vtWvObs; }
      set { vtWvObs = value; }
    }

    public double DfObs
    {
      get { return dfObs; }
      set { dfObs = value; }
    }

    public double S0Obs
    {
      get { return s0Obs; }
      set { s0Obs = value; }
    }

    // retorna o numero total de angulos
    public int NDAngs
    {
      get 
	{
	  int count = 0;
	  foreach (SpinDAng ss in spinDAngs)
	    {
	      count += ss.Spin.Count;
	    }
	  return count;
	}
    }

    // o numero de distancias
    public int NDDists
    {
      get { return dDists.Count; }
    }

    // o numero de ln distancias
    public int NDLnDists
    {
      get { return dLnDists.Count; }
    }

    // retorna o numero total de quocientes
    public int NDQuos
    {
      get 
	{
	  int count = 0;
	  foreach (SpinDQuo ss in spinDQuos)
	    {
	      count += ss.Spin.Count;
	    }
	  return count;
	}
    }


    // FUNCOES

    public void AddSpinDAng (SpinDAng newSpinDAng)
    {
      spinDAngs.Add(newSpinDAng);
    }

    public void AddDDist (DDist newDDist)
    {
      dDists.Add(newDDist);
    }

    public void AddDLnDist (DLnDist newDLnDist)
    {
      dLnDists.Add(newDLnDist);
    }

    public void AddSpinDQuo (SpinDQuo newSpinDQuo)
    {
      spinDQuos.Add(newSpinDQuo);
    }

  }

}