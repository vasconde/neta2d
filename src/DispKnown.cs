//  DispKnown.cs - Equacao de constragimento do tipo deslocamento conhecido
//
//  Copyright 2013 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;
using System.Collections.Generic; // list

namespace NetA2D
{

  public class DispKnown
  {

    Point p;
    int componente;
    double displacement;
    double stDev;

    double residual;

    // get and set

    public Point P
    {
      get { return p; }
      set { p = value; }
    }

    public int Componente
    {
      get { return componente; }
      set { componente = value; }
    }
    
    public double Displacement
    {
      get { return displacement; }
      set { displacement = value; }
    }

    public double StDev
    {
      get { return stDev; }
      set { stDev = value; }
    }

    public double Residual
    {
      get { return residual; }
      set { residual = value; }
    }

    // construtor
    
    public DispKnown(Point p, int componente, double displacement,
		     double stDev)
    {
      this.p = p;
      this.componente = componente;
      this.displacement = displacement;
      this.stDev = stDev;


    }

  }

}