//  ConstrainEq.cs - Equacoes de constragimento
//
//  Copyright 2013, 2014 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;
using System.Collections.Generic; // list

namespace NetA2D
{

  public class ConstraintEq
  {
    List <DispKnown> dispKnowns; // deslocamento conhecido (e.g. dX = 0)
    List <SumPair> sumPairs; // soma de pares conhecida (e.g. dX1+dX2 = 0)

    double vtWvEqc;
    double dfEqc;
    double s0Eqc;

    // get and set

    /// <summary>
    ///   Lista de componentes de referencia "compoentes fixas"
    /// </summary>
    public List <DispKnown> DispKnowns
    {
      get { return dispKnowns; }
      set { dispKnowns = value; }
    }

    /// <summary>
    ///   Lista de equacoes do tipo (dX1 + dX2 = 0)
    /// </summary>
    public List <SumPair> SumPairs
    {
      get { return sumPairs; }
      set { sumPairs = value; }
    }

    /// <summary>
    ///   Retorna o numero de equacoes de condicao
    ///   do tipo DispKnown
    /// </summary>
    public int NDispKnowns
    {
      get
	{
	  return dispKnowns.Count;
	}
    }

    /// <summary>
    ///   Retorna o numero de equacoes de condicao
    ///   do tipo SumPairs
    /// </summary>
    public int NSumPairs
    {
      get
	{
	  return sumPairs.Count;
	}
    }

    public double VtWvEqc
    {
      get { return vtWvEqc; }
      set { vtWvEqc = value; }
    }

    public double DfEqc
    {
      get { return dfEqc; }
      set { dfEqc = value; }
    }

    public double S0Eqc
    {
      get { return s0Eqc; }
      set { s0Eqc = value; }
    }

    // CONSTRUTOR

    /// <summary>
    ///   Construtor de ConstraintEq
    /// </summary>
    public ConstraintEq()
    {
      dispKnowns = new List <DispKnown>();
      sumPairs = new List <SumPair>();
    }

    // METODOS

    /// <summary>
    ///   Adiciona uma nova equacao de condicao
    /// </summary>
    public void AddDispKnown (DispKnown newDispKnown)
    {
      dispKnowns.Add(newDispKnown);
    }

    /// <summary>
    ///   Adiciona uma nova equacao de condicao 
    /// </summary>
    public void AddSumPair (SumPair newSumPair)
    {
      sumPairs.Add(newSumPair);
    }

  }

}