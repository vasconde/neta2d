//  SumPair.cs - Equacao de constragimento do tipo soma de duas componentes
//               nula (dx1 + dx2 = 0)
//
//  Copyright 2013 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;
using System.Collections.Generic; // list

namespace NetA2D
{

  public class SumPair
  {

    Point pointFirst;
    int compFirst; // 0 se X, 1 se Y
    int signalFirst; // 0 se +, 1 se -

    Point pointSecond;
    int compSecond; // 0 se X, 1 se Y
    int signalSecond; // 0 se +, 1 se -

    double sumResult; // por defeito sera igual a zero

    double stdDev;

    double residual;

    // GET AND SET

    public Point PointFirst
    {
      get { return pointFirst; }
      set { pointFirst = value; }
    }

    public int CompFirst
    {
      get { return compFirst; }
      set { compFirst = value; }
    }

    public int SignalFirst
    {
      get { return signalFirst; }
      set { signalFirst = value; }
    }

    public Point PointSecond
    {
      get { return pointSecond; }
      set { pointSecond = value; }
    }

    public int CompSecond
    {
      get { return compSecond; }
      set { compSecond = value; }
    }

    public int SignalSecond
    {
      get { return signalSecond; }
      set { signalSecond = value; }
    }

    public double StdDev
    {
      get { return stdDev; }
      set { stdDev = value; }
    }

    public double SumResult
    {
      get { return sumResult; }
      set { sumResult = value; }
    }


    public double Residual
    {
      get { return residual; }
      set { residual = value; }
    }


    // CONSTRUTOR

    /// <summary>
    ///   Equacao do tipo: dX1 + dX2 = 0
    ///   Parametros: ponto, componente (0-X,1-Y), sinal (0-+,1--)
    /// </summary>
    public SumPair(Point pointFirst, int compFirst, int signalFirst,
		   Point pointSecond, int compSecond, int signalSecond,
		   double stdDev)
    {
      this.pointFirst = pointFirst;
      this.compFirst = compFirst;
      this.signalFirst = signalFirst;

      this.pointSecond = pointSecond;
      this.compSecond = compSecond;
      this.signalSecond = signalSecond;
      this.stdDev = stdDev;

      this.sumResult = 0;
    }

  }

}