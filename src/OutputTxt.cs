//  OutputTxt.cs - Escreve os resultados num ficheiro de texto
//
//  Copyright 2013, 2014, 2015 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;

using System.IO; // ler e escrever ficheiro
using System.Collections.Generic; // list

namespace NetA2D
{

  public class OutputTxt
  {

    string filePath;
    Network n;
    Obs o;
    ConstraintEq c;
    List<string> notas;
    bool dtdr;
    bool dxtNdx;
    bool dxtdx;

    int alinea;
    
    StreamWriter swResults;

    string separador = "********************************************************************************";

    string versao = "1.1";

    public OutputTxt (string filePath, Network n, Obs o, ConstraintEq c,
		      List<string> notas, bool dtdr = false, bool dxtdx = false,
		      bool dxtNdx = false)
    {
      this.filePath = filePath;
      this.n = n;
      this.o = o;
      this.c = c;
      this.notas = notas;
      this.dtdr = dtdr;
      this.dxtNdx = dxtNdx;
      this.dxtdx = dxtdx;

      alinea = 1;
    }

    public bool Dtdr
    {
      get { return dtdr; }
      set { dtdr = value; }
    }

    public bool DxtNdx
    {
      get { return dxtNdx; }
      set { dxtNdx = value; }
    }

    public bool Dxtdx
    {
      get { return dxtdx; }
      set { dxtdx = value; }
    }

    public void WriteResults()
    {
      swResults = new StreamWriter(filePath);

      // cabecalho
      Head ();

      if (notas != null && notas.Count > 0)
	{
	  swResults.WriteLine();
	  Descricao ();
	}

      swResults.WriteLine();
      Info();

      swResults.WriteLine();
      Deslocamentos();

      swResults.WriteLine();
      Observacoes();

      if (dxtNdx || dxtdx)
	{
	  swResults.WriteLine();
	  Outros ();
	}

      swResults.WriteLine();
      Foot ();

      swResults.Close();
    }

    // imprime o cabecalho
    void Head ()
    {
      swResults.WriteLine(separador);
      swResults.WriteLine("                                     NetA2D");
      swResults.WriteLine("                                  Versão: {0}", versao);
      swResults.WriteLine(separador);
      swResults.WriteLine("                             FICHEIRO DE RESULTADOS");
      swResults.WriteLine("                               {0}", DateTime.Now);
      swResults.WriteLine(separador);
    }

    // fica por fazer
    void Descricao ()
    {
      swResults.WriteLine(" {0}) Notas", alinea);
      swResults.WriteLine();
      foreach (string nota in notas)
	{
	  swResults.WriteLine(" " + nota);
	}

      alinea++;
    }

    void Info()
    {

      int ni = n.NPointsComponents; // numero de incognitas
      int neq = o.NDAngs+o.NDDists+o.NDLnDists+o.NDQuos; // numero de equacoes
      int nobs = o.NDAngs+o.NDDists+o.NDLnDists+o.NDQuos;
      
      // defeciencia de caracteristica
      int defcat = 0;
      if (o.NDAngs != 0)
	{
	  defcat = 4;
	}
      // se tiver distancias passa a 3
      if (o.NDDists != 0)
	{
	  defcat = 3;
	}
      
      int df = nobs-ni+defcat;        // NOTA: o df nao conta com as eq. de cond.
      
      double rr = df*1.0/nobs; // NOTA: o df nao conta com as eq. de cond.

      swResults.WriteLine(" {0}) Informação", alinea);
      swResults.WriteLine();

      swResults.WriteLine(" Número de incógnitas (n): ......................... {0}", ni);
      swResults.WriteLine();
      swResults.WriteLine(" Número de equações de observação (m): ............. {0}", neq);
      
      if ( o.NDAngs > 0)
	swResults.WriteLine("   angulares (ma): ................................. {0}",o.NDAngs);

      if ( o.NDDists > 0 )
	swResults.WriteLine("   lineares (ml): .................................. {0}",o.NDDists);

      if ( o.NDLnDists > 0 )
	swResults.WriteLine("   de diferença de logaritmos: ... {0}",o.NDLnDists);

      if ( o.NDQuos > 0 )
	swResults.WriteLine("   de quocientes: ................ {0}",o.NDQuos);

      swResults.WriteLine();
      swResults.WriteLine(" Deficiência de característica (r): ................ {0}", defcat);

      swResults.WriteLine();
      swResults.WriteLine(" Número de equações de condição (r+k): ............. {0}", c.NDispKnowns+c.NSumPairs);

      swResults.WriteLine();
      swResults.WriteLine(" Redundância média ((m-n+r)/m): .................... {0,-10:F3}", rr);

      alinea++;
    }

    void Deslocamentos ()
    {
      swResults.WriteLine(" {0}) Deslocamentos", alinea);
      swResults.WriteLine();
      
      int nalinea = 1;

      // verificacao se existem realmente pontos
      // com componentes tagenciais e radiais antes de tudo
      if (dtdr)
	{
	  dtdr = false;
	  foreach(Point p in n.Points)
	    {
	      if(p.ThetaThere)
		dtdr = true;
	    }
	}

      if (dtdr)
	{
	  swResults.WriteLine(" {0}.{1}) Componentes dX e dY", alinea, nalinea);
	  swResults.WriteLine();
	  nalinea++;
	}

      swResults.WriteLine(" {0,-11}  {1,11}{2,11}", "Vértice   ", "dX", "dY");
      swResults.WriteLine(" -----------------------------------");
      foreach(Point p in n.Points)
	{
	  swResults.WriteLine(" {0,-11}  {1,11:F1}{2,11:F1}",
			      p.Name, p.DX, p.DY);
	}
      swResults.WriteLine(" -----------------------------------");

      if (dtdr)
	{
	  swResults.WriteLine();
	  swResults.WriteLine(" {0}.{1}) Componentes dT e dR", alinea, nalinea);
	  swResults.WriteLine();
	  nalinea++;

	  swResults.WriteLine(" {0,-11}  {1,11}{2,11}", "Vértice   ", "dT", "dR");
	  swResults.WriteLine(" -----------------------------------");
	  foreach(Point p in n.Points)
	    {
	      if(p.ThetaThere)
		swResults.WriteLine(" {0,-11}  {1,11:F1}{2,11:F1}",
				    p.Name, p.DT, p.DR);
	    }
	  swResults.WriteLine(" -----------------------------------");
	  
	}

      alinea++;
    }
    
    void Observacoes()
    {
      swResults.WriteLine(" {0}) Ajustamento", alinea);

      int nalinea = 1;

      // ANGULOS
      if (o.NDAngs > 0)
	{
	  swResults.WriteLine();
	  swResults.WriteLine(" {0}.{1}) Diferenças de ângulos", alinea, nalinea);
	  swResults.WriteLine();
	  swResults.WriteLine(" {0,-11}{1,-11}{2,-11}  {3,12}{4,12}{5,12}",
			      "origem", "estação", "visado", "leitura",
			       "ajustada","resíduo");
	  swResults.WriteLine(" -----------------------------------------------------------------------");

	  foreach(SpinDAng sss in o.SpinDAngs)
	    {
	      swResults.WriteLine();
	      swResults.WriteLine(" DP da variação de direção: {0:F1}", sss.StdObsA);
	      swResults.WriteLine(" -");
	      foreach(DAng dang in sss.Spin)
		{
		  swResults.WriteLine(" {0,-11}{1,-11}{2,-11}  {3,12:F1}{4,12:F1}{5,12:F1}",
				      dang.Origin.Name, dang.Occupied.Name,
				      dang.Sighted.Name,
				      dang.Obs, dang.Obs + dang.Residual,
				      dang.Residual);
		}
	    }
	  swResults.WriteLine(" -----------------------------------------------------------------------");
	  
	  swResults.WriteLine();
	  swResults.WriteLine(" vT P v  : {0,10:F3}", o.VtWvDAng);
	  swResults.WriteLine(" tr(I-U) : {0,10:F3}", o.DfDAng);
	  swResults.WriteLine(" sA      : {0,10:F3}", o.S0DAng);

	  nalinea++;
	}

	 
      // DISTANCIAS
      if (o.NDDists > 0)
	{
	  swResults.WriteLine();
	  swResults.WriteLine(" {0}.{1}) Diferenças de distâncias", alinea, nalinea);
	  swResults.WriteLine();
	  swResults.WriteLine(" {0,-11}{1,-11}  {2,12}{3,12}{4,12}{5,12}",
			      "estação", "visado", "DP ", "leitura",
			      "ajustada", "resíduo");
	  swResults.WriteLine(" ------------------------------------------------------------------------");
	  foreach(DDist ddist in o.DDists)
	    {
	      swResults.WriteLine(" {0,-11}{1,-11}  {2,12:F1}{3,12:F1}{4,12:F1}{5,12:F1}",
				  ddist.Occupied.Name, ddist.Sighted.Name,
				  ddist.StdObsA,ddist.Obs,
				  ddist.Obs + ddist.Residual, ddist.Residual);
	    }
	  swResults.WriteLine(" ------------------------------------------------------------------------");
	  

	  swResults.WriteLine();
	  swResults.WriteLine(" vT P v  : {0,10:F3}", o.VtWvDDist);
	  swResults.WriteLine(" tr(I-U) : {0,10:F3}", o.DfDDist);
	  swResults.WriteLine(" sL      : {0,10:F3}", o.S0DDist);

	  nalinea++;
	}

      // LOG DISTANCIAS (LN)
      if (o.NDLnDists > 0)
	{
	  swResults.WriteLine();
	  swResults.WriteLine(" {0}.{1}) Diferenças de distâncias", alinea, nalinea);
	  swResults.WriteLine();
	  swResults.WriteLine(" {0,-10}{1,-10}  {2,10}{3,10}{4,10}",
			      "estação", "visado", "DP ", "leitura",
			      "resíduo");
	  swResults.WriteLine(" ----------------------------------------------------");
	  foreach(DLnDist dlndist in o.DLnDists)
	    {
	      swResults.WriteLine(" {0,-10}{1,-10}  {2,10:F1}{3,10:F1}{4,10:F1}",
				  dlndist.Occupied.Name, dlndist.Sighted.Name,
				  dlndist.StdObsA,dlndist.LnObs, dlndist.Residual);
	    }
	  swResults.WriteLine(" ----------------------------------------------------");
	  

	  swResults.WriteLine();
	  swResults.WriteLine(" vt P v  : {0,10:F3}", o.VtWvDLnDist);
	  swResults.WriteLine(" tr(I-U) : {0,10:F3}", o.DfDLnDist);
	  swResults.WriteLine(" sLL     : {0,10:F3}", o.S0DLnDist);

	  nalinea++;
	}

      // Quocientes (inicio)
      if (o.NDQuos > 0)
	{
	  swResults.WriteLine();
	  swResults.WriteLine(" {0}.{1}) Quocientes", alinea, nalinea);
	  swResults.WriteLine();
	  swResults.WriteLine(" {0,-10}{1,-10}{2,-10}  {3,10}{4,10}{5,10}",
			      "origem", "estação", "visado", "DP ", "leitura",
			      "resíduo");
	  swResults.WriteLine(" --------------------------------------------------------------");

	  foreach(SpinDQuo sss in o.SpinDQuos)
	    {
	      swResults.WriteLine(" *");
	      foreach(DQuo dquo in sss.Spin)
		{
		  swResults.WriteLine(" {0,-10}{1,-10}{2,-10}  {3,10:F1}{4,10:F1}{5,10:F1}",
				      dquo.Origin.Name, dquo.Occupied.Name,
				      dquo.Sighted.Name, dquo.StdObsA,
				      dquo.ObsDLnQuo, dquo.Residual);
		}
	    }
	  swResults.WriteLine(" --------------------------------------------------------------");
	  
	  swResults.WriteLine();
	  swResults.WriteLine(" vT P v  : {0,10:F3}", o.VtWvDQuo);
	  swResults.WriteLine(" tr(I-U) : {0,10:F3}", o.DfDQuo);
	  swResults.WriteLine(" sQL     : {0,10:F3}", o.S0DQuo);

	  nalinea++;
	}  // Quocientes (fim)


      // estatisticas com todas as obs

      swResults.WriteLine();
      swResults.WriteLine(" vT P v  : {0,10:F3}", o.VtWvObs);
      swResults.WriteLine(" tr(I-U) : {0,10:F3}", o.DfObs);
      swResults.WriteLine(" sAL     : {0,10:F3}", o.S0Obs);


      swResults.WriteLine();
      swResults.WriteLine(" {0}.{1}) Equações de condição", alinea, nalinea);
      

// equacoes de condicao

      if(c.NDispKnowns > 0)
	{
	  swResults.WriteLine();
	  swResults.WriteLine(" {0,-11}{1,-11}  {2,11}{3,11}{4,11}",
			      "vértice", "componente", "DP ", "leitura",
			      "resíduo");
	  swResults.WriteLine(" ---------------------------------------------------------");

	  foreach(DispKnown dk in c.DispKnowns)
	    {

	      if(dk.Componente == 0) // ou seja X
		{
		  swResults.WriteLine(" {0,-11}{1,-11}  {2,11:F1}{3,11:F1}{4,11:F1}",
				      dk.P.Name, "dX", dk.StDev,
				      dk.Displacement, dk.Residual);
		}
	      if(dk.Componente == 1) // ou seja Y
		{
		  swResults.WriteLine(" {0,-11}{1,-11}  {2,11:F1}{3,11:F1}{4,11:F1}",
				      dk.P.Name, "dY", dk.StDev,
				      dk.Displacement, dk.Residual);
		}
	    }
	  swResults.WriteLine(" ---------------------------------------------------------");

	}

      if(c.NSumPairs > 0)
	{
	  swResults.WriteLine();
	  swResults.WriteLine(" {0,-11}{1,-11}{2,-11}  {3,11}{4,11}",
			      "vértice1", "vértice2", "Equação", "DP ",
			      "resíduo");
	  swResults.WriteLine(" ---------------------------------------------------------");
	  foreach(SumPair sp in c.SumPairs)
	    {
	      string s1 =  sp.SignalFirst == 0 ? "" : "-";
	      string c1 = sp.CompFirst == 0 ? "dX1" : "dY1";

	      string s2 = sp.SignalSecond == 0 ? "+" : "-";
	      string c2 = sp.CompSecond == 0 ? "dX2" : "dY2";

	      string equacao = s1+c1+s2+c2+"=0";

	      swResults.WriteLine(" {0,-11}{1,-11}{2,-11}  {3,11:F1}{4,11:F1}",
				  sp.PointFirst.Name, sp.PointSecond.Name,
				  equacao, sp.StdDev, sp.Residual);
	    }

	  swResults.WriteLine(" ---------------------------------------------------------");

	}

      swResults.WriteLine();
      swResults.WriteLine(" vT P v  : {0,10:F3}", c.VtWvEqc);
      swResults.WriteLine(" tr(I-U) : {0,10:F3}", c.DfEqc);
      swResults.WriteLine(" sC      : {0,10:F3}", c.S0Eqc);


      // // TENDO EM CONTA A DEFICIENCIA DE CARACTEristica
      // // se forem so angulos

      // int nobs = o.NDAngs+o.NDDists+o.NDLnDists+o.NDQuos;

      // if (nobs == o.NDAngs)
      // 	{
      // 	  if (c.NDispKnowns + c.NSumPairs > 4) // def carac. = 4
      // 	    {
      // 	      swResults.WriteLine();
      // 	      swResults.WriteLine(" vT P v  : {0,10:F3}", c.VtWvEqc);
      // 	      swResults.WriteLine(" tr(I-U) : {0,10:F3}", c.DfEqc);
      // 	      swResults.WriteLine(" sC      : {0,10:F3}", c.S0Eqc);
      // 	    }
      // 	}
      // // se nao forem so angulos ou se forem so distancias
      // else if (nobs > o.NDAngs || nobs == o.NDDists)
      // 	{
      // 	  if (c.NDispKnowns + c.NSumPairs > 3) // def carac. = 3
      // 	    {
      // 	      swResults.WriteLine();
      // 	      swResults.WriteLine(" vT P v  : {0,10:F3}", c.VtWvEqc);
      // 	      swResults.WriteLine(" tr(I-U) : {0,10:F3}", c.DfEqc);
      // 	      swResults.WriteLine(" sC      : {0,10:F3}", c.S0Eqc);
      // 	    }
      // 	}

      

      nalinea++;

      swResults.WriteLine();
      swResults.WriteLine(" {0}.{1}) DP da unidade de peso, a posteriori", alinea, nalinea);
      swResults.WriteLine();
      swResults.WriteLine(" vT P v  : {0,10:F3}", o.VtWv);
      swResults.WriteLine(" tr(I-U) : {0,10:F3}", o.Df);
      swResults.WriteLine(" s0      : {0,10:F3}", o.S0);

      nalinea++;


      alinea++;

    }

    void Outros ()
    {
      int nalinea = 1;

      swResults.WriteLine(" {0}) Outros", alinea);

      if(dxtdx)
	{ 
	  swResults.WriteLine();
	  swResults.WriteLine(" {0}.{1}) Quadrado da norma do vetor de deslocamentos",
			      alinea, nalinea);
	  
	  swResults.WriteLine();
	  swResults.WriteLine(" dXT dX : {0,-10:F3}", n.DxtDx);
	  
	  nalinea++;
	}

      if(dxtNdx)
	{ 
	  swResults.WriteLine();
	  swResults.WriteLine(" {0}.{1}) Quadrado da norma N do vetor de deslocamentos",
			      alinea, nalinea);
      
	  swResults.WriteLine();
	  swResults.WriteLine(" dXT N dX : {0,-10:F3}", n.DxtNDx);

	  nalinea++;
	}

      alinea++;
    }

    // imprime o cabecalho
    void Foot ()
    {
      swResults.WriteLine(separador);
      swResults.WriteLine("NetA2D Versão: {0}", versao);
      
    }


  }

}