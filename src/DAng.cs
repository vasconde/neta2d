//  DAng.cs - Observcoes do tipo diferencas de angulos
//
//  Copyright 2013 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;


namespace NetA2D
{

  public class DAng
  {
    Point origin;
    Point occupied;
    Point sighted;

    double obs;

    double stdObsA; // standard deviation apriori

    double residual;


    // GET AND SET

    public Point Origin
    {
      get { return origin; }
      set { occupied = value; }
    }    

    public Point Occupied
    {
      get { return occupied; }
      set { occupied = value; }
    }

    public Point Sighted
    {
      get { return sighted; }
      set { sighted = value; }
    }

    public double Obs
    {
      get { return obs; }
      set { obs = value; }
    }

    public double StdObsA
    {
      get { return stdObsA; }
      set { stdObsA = value; }
    }

    public double Residual
    {
      get { return residual; }
      set { residual = value; }
    }

    // construtores
  

    public DAng (Point origin, Point occupied, Point sighted, double obs,
		 double stdObsA)
    {
      this.origin = origin;
      this.occupied = occupied;
      this.sighted = sighted;
      this.obs = obs;
      this.stdObsA = stdObsA;
    }

    public DAng (Point origin, Point occupied, Point sighted, double obs)
    {
      this.origin = origin;
      this.occupied = occupied;
      this.sighted = sighted;
      this.obs = obs;
    }

    // FUNCOES

    // derivadas em ordem as coordenadas
    // o retorno eh um vetor [d/dx_o, d/dy_o, d/dx_e, d/dy_e, d/dx_v, d/dy_v]
    public double[] Derivatives ()
    {
      double[] derivatives = new double[6];

      // occupied - origin
      double difXOcO = origin.X - occupied.X;
      double difYOcO = origin.Y - occupied.Y;
      double distOcO2 = Math.Pow(difXOcO,2) + Math.Pow(difYOcO,2);

      // occupied - sighted
      double difXOcS = sighted.X - occupied.X;
      double difYOcS = sighted.Y - occupied.Y;
      double distOcS2 = Math.Pow(difXOcS,2) + Math.Pow(difYOcS,2);

      // origin
      derivatives[0] = - difYOcO / distOcO2; // d_dxo
      derivatives[1] = difXOcO / distOcO2; // d_dyo

      // sighted
      derivatives[4] = difYOcS / distOcS2; // d_dxs
      derivatives[5] = - difXOcS / distOcS2; // d_dys

      // occupied
      // ATENCAO: REPAREM NOS INDICES DAS DERIVADAS
      derivatives[2] = - derivatives[0] - derivatives[4]; // d_dxoc
      derivatives[3] = - derivatives[5] - derivatives[1]; // d_dyoc
      
      return derivatives;
    }




  }
}