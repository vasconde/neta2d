//  SpinDQuo.cs - Giro do horizonte (quocientes de ln distancia numa mesma estacao)
//
//  Copyright 2013 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;
using System.Collections.Generic; // list

namespace NetA2D
{

  public class SpinDQuo
  {
    List <DQuo> spin;
    
    Point occupied;
    Point origin;

    double dDist;

    double stdObsA; // standard deviation apriori

    // GET and SET

    public List<DQuo> Spin
    {
      get { return spin; }
      set { spin = value; }
    }

    // retorna o numero de dangs no spin
    public int NSpin
    {
      get { return spin.Count; }
    }

    public Point Occupied
    {
      get { return occupied; }
      set { occupied = value; }
    }

    public Point Origin
    {
      get { return origin; }
      set { origin = value; }
    }

    public double DDist
    {
      get { return dDist; }
      set { dDist = value; }
    }


    public double StdObsA
    {
      get { return stdObsA; }
      set { stdObsA = value; }
    }

    public Matrix WMatrix
    {
      get
	{
	  return ComputeWMatrix ();
	}
    }

    // CONSTRUTOR

    public SpinDQuo(Point occupied, Point origin, double dDist, double stdObsA)
    {

      spin = new List <DQuo>();

      this.origin = origin;
      this.occupied = occupied;

      // diferenca de distancia
      this.dDist = dDist;

      this.stdObsA = stdObsA;
    }

    // FUNCOES

    public bool AddDQuo (DQuo dang)
    {
      if(dang.Origin == origin && dang.Occupied == occupied)
	{
	  spin.Add(dang);
	  return true;
	}
      return false;
    }

    // calcula a matriz covariancia das angulos do giro
    Matrix ComputeWMatrix ()
    {
      Matrix wMatrix = new Matrix(spin.Count, spin.Count, 2);

      for (int i = 0; i < spin.Count; i++)
	{
	  for(int j = 0; j < spin.Count; j++)
	    {
	      if(i != j)
		{
		  wMatrix[i,j] = 1;
		}
	    }
	}

      wMatrix = wMatrix.Inverse;

      for (int i = 0; i < spin.Count; i++)
	{
	  for(int j = 0; j < spin.Count; j++)
	    {
	      wMatrix[i,j] = (1.0 / Math.Pow(stdObsA,2)) * wMatrix[i,j];
	    }
	}
      
      return wMatrix;

      // Console.WriteLine(wMatrix);
    }




  }

}