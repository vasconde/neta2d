//  Network.cs - Vertices da rede
//
//  Copyright 2013, 2014 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;
using System.Collections.Generic; // list

namespace NetA2D
{

  public class Network
  {
    List<Point> points;

    public Network()
    {
      points = new List<Point>();
    }

    // quadrado da norma N do vetor de deslocamentos
    double dxtNDx;

    // quadrado da norma N do vetor de deslocamentos
    double dxtDx;

    // GET and SET

    /// <summary>
    ///   Lista de pontos
    /// </summary>
    public List<Point> Points
    {
      get { return points; }
      set { points = value; }
    }

    /// <summary>
    ///   Retorna o numero de componentes (2*npontos)
    /// </summary>
    public int NPointsComponents
    {
      get 
	{ 
	  return 2 * points.Count;
	}
    }
  
    /// <summary>
    ///   Numero de pontos
    /// </summary>
    public int NPoints
    {
      get { return points.Count; }
    }

    public double DxtNDx
    {
      get { return dxtNDx; }
      set { dxtNDx = value; }
    }

    public double DxtDx
    {
      get { return dxtDx; }
      set { dxtDx = value; }
    }
    
    /// <summary>
    ///   Retorna o Point atraves do seu nome
    /// </summary>
    public Point PointByName (string name)
    {
      foreach (Point p in points)
	{
	  if( p.Name == name )
	    {
	      return p;
	    }
	}
      return null;
    }

    /// <summary>
    ///   Adiciona um ponto. Retorna false se ja existir um ponto
    ///   com esse nome
    /// </summary>
    public bool AddPoint (string name, double x, double y)
    {
      // se nao existe um ponto com este nome
      if( PointByName (name) == null )
	{
	  points.Add(new Point (name, x, y));
	  return true;
	}
      else
	{
	  return false;
	}
    }
    
    /// <summary>
    ///   Adiciona um ponto. Retorna false se ja existir um ponto
    ///   com esse nome
    /// </summary>
    public bool AddPoint (string name, double x, double y, double theta)
    {
      // se nao existe um ponto com este nome
      if( PointByName (name) == null )
	{
	  points.Add(new Point (name, x, y, theta));
	  return true;
	}
      else
	{
	  return false;
	}
    }

    /// <summary>
    ///   Remove um ponto. Retorna false se nao existir um ponto
    ///   com esse nome
    /// </summary>
    public bool RemovePointByName(string name)
    {
      Point p = PointByName (name);

      if ( p != null )
	{
	  points.Remove(p);
	  return true;
	}
      else
	{
	  return false;
	}
    }
 


  }

}