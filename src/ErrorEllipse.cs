//  ErrorEllipse.cs - Calcula as componentes das elipses de erro
//
//  Copyright 2013 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;

namespace NetA2D
{

  public class ErrorEllipse
  {
    public static void Apriori (double prob, Network n, Matrix A, Matrix W)
    {

      Matrix N = A.Transpose() * W * A;
      Matrix invN = N.Inverse;

      // variaveis de apoio
      // semi eixo maior, semi eixo menor e azimute
      double ma2, me2, az_ma;

      double sigma2_x, sigma2_y, sigma_xy;
      
      // for(int i = 0; i < A.Columns; i += 2)
      // 	{
      // 	  sigma2_x = invN[i,i];
      // 	  sigma2_y = invN[i+1,i+1];
      // 	  sigma_xy = invN[i,i+1];

      // 	  ma2 = 2.0 * prob * (sigma2_x*sigma2_y-Math.Pow(sigma_xy,2)) /
      // 	    (sigma2_x + sigma2_y - 
      // 	     Math.Sqrt( Math.Pow(sigma2_x - sigma2_y,2)
      // 			+ 4*Math.Pow(sigma_xy,2) ));

      // 	  me2 = 2.0 * prob * (sigma2_x*sigma2_y-Math.Pow(sigma_xy,2)) /
      // 	    (sigma2_x + sigma2_y + 
      // 	     Math.Sqrt( Math.Pow(sigma2_x - sigma2_y,2)
      // 			+ 4*Math.Pow(sigma_xy,2) ));

      // 	  az_ma = Math.Atan((sigma2_y - sigma2_x +
      // 			    Math.Sqrt( Math.Pow(sigma2_x - sigma2_y,2)
      // 				       + 4*Math.Pow(sigma_xy,2) )) /
      // 			    (2*sigma_xy));

      // 	  parameters[j,0] = Math.Sqrt(ma2);
      // 	  j++;
      // 	  parameters[j,0] = Math.Sqrt(me2);
      // 	  j++;
      // 	  parameters[j,0] = az_ma;
      // 	  j++;

      // 	  Console.WriteLine(Convert.ToString(Math.Sqrt(ma2))
      // 			    + "   " + Convert.ToString(Math.Sqrt(me2))
      // 			    + "   " + Convert.ToString(az_ma));
      // 	}

      int i = 0;
      foreach(Point p in n.Points)
	{
	  sigma2_x = invN[i,i];
	  sigma2_y = invN[i+1,i+1];
	  sigma_xy = invN[i,i+1];

	  ma2 = 2.0 * prob * (sigma2_x*sigma2_y-Math.Pow(sigma_xy,2)) /
	    (sigma2_x + sigma2_y - 
	     Math.Sqrt( Math.Pow(sigma2_x - sigma2_y,2)
			+ 4*Math.Pow(sigma_xy,2) ));

	  me2 = 2.0 * prob * (sigma2_x*sigma2_y-Math.Pow(sigma_xy,2)) /
	    (sigma2_x + sigma2_y + 
	     Math.Sqrt( Math.Pow(sigma2_x - sigma2_y,2)
			+ 4*Math.Pow(sigma_xy,2) ));

	  az_ma = Math.Atan((sigma2_y - sigma2_x +
			    Math.Sqrt( Math.Pow(sigma2_x - sigma2_y,2)
				       + 4*Math.Pow(sigma_xy,2) )) /
			    (2*sigma_xy));

	  p.SetErrorEllip (prob, Math.Sqrt(ma2), Math.Sqrt(me2), az_ma);

	  // Console.WriteLine(Convert.ToString(Math.Sqrt(ma2))
	  // 		    + "   " + Convert.ToString(Math.Sqrt(me2))
	  // 		    + "   " + Convert.ToString(az_ma));
	  i += 2;
	}

    }
  }

}