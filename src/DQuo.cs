//  DDist.cs - Observcoes do tipo diferencas de distancias
//
//  Copyright 2013 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;


namespace NetA2D
{

  public class DQuo
  {

    // giro ao qual o quo esta associado
    SpinDQuo mySpin;

    Point origin;
    Point occupied;

    Point sighted;

    // diferenca de distancia
    double obsDDist;

    // observacao processada em quociente
    double obsDLnQuo;

    double stdObsA; // standard deviation apriori

    double residual;


    // GET AND SET

    public SpinDQuo MySpin
    {
      get { return mySpin; }
      set { mySpin = value; }
    }

    public Point Origin
    {
      get { return origin; }
      set { occupied = value; }
    }    

    public Point Occupied
    {
      get { return occupied; }
      set { occupied = value; }
    }

    public Point Sighted
    {
      get { return sighted; }
      set { sighted = value; }
    }

    // diferenca de distancia
    public double ObsDDist
    {
      get { return obsDDist; }
      set { obsDDist = value; }
    }

    // observacao processada em quociente
    public double ObsDLnQuo
    {
      get { return obsDLnQuo; }
      set { obsDLnQuo = value; }
    }

    public double StdObsA
    {
      get { return stdObsA; }
      set { stdObsA = value; }
    }

    public double Residual
    {
      get { return residual; }
      set { residual = value; }
    }

    // construtores
  

    public DQuo (SpinDQuo mySpin, Point sighted,
		 double obsDDist, double stdObsA)
    {
      this.mySpin = mySpin;

      this.origin = mySpin.Origin;
      this.occupied = mySpin.Occupied;
      this.sighted = sighted;

      this.obsDDist = obsDDist;

//      this.obs = obs;

      this.stdObsA = stdObsA;

      UpdateDLnQuo();
    }

    public DQuo (SpinDQuo mySpin, Point sighted, double obsDDist)
    {
      this.mySpin = mySpin;

      this.origin = mySpin.Origin;
      this.occupied = mySpin.Occupied;
      this.sighted = sighted;

      this.obsDDist = obsDDist;

//      this.obs = obs;

      UpdateDLnQuo();
    }

    // FUNCOES

    public double DifXOO ()
    {
      return origin.X - occupied.X;
    }

    public double DifYOO ()
    {
      return origin.Y - occupied.Y;
    }


    public double DifXOS ()
    {
      return sighted.X - occupied.X;
    }

    public double DifYOS ()
    {
      return sighted.Y - occupied.Y;
    }


    // Retorna a distancia aproximada calculada a partir
    // das coordenadas aproximadas
    public double AproxDistOO()
    {
      return Math.Sqrt( Math.Pow(DifXOO(),2) + Math.Pow(DifYOO(),2) );
    }

    // Retorna a distancia aproximada calculada a partir
    // das coordenadas aproximadas
    public double AproxDistOS()
    {
      return Math.Sqrt( Math.Pow(DifXOS(),2) + Math.Pow(DifYOS(),2) );
    }

    // gera leitura ln obs
    public void UpdateDLnQuo()
    {

      double part1 = 1.0 / AproxDistOO() * this.obsDDist;
      double part2 = - AproxDistOS() / Math.Pow(AproxDistOO(),2) * this.mySpin.DDist;

      double dQuo = part1 + part2;

      this.obsDLnQuo = (dQuo / ( AproxDistOS() / AproxDistOO() )) * 2000000.0/Math.PI;
    }

    
    private double[] DerivativesAuxOO ()
    {
      double[] derivatives = new double[4];

      derivatives[0] = -DifXOO () / Math.Pow(AproxDistOO(), 2); // d_dxa
      derivatives[1] = -DifYOO () / Math.Pow(AproxDistOO(), 2); // d_dya
      derivatives[2] = DifXOO ()  / Math.Pow(AproxDistOO(), 2); // d_dxb
      derivatives[3] = DifYOO ()  / Math.Pow(AproxDistOO(), 2); // d_dyb
      
      return derivatives;
    }

    private double[] DerivativesAuxOS ()
    {
      double[] derivatives = new double[4];

      derivatives[0] = -DifXOS () / Math.Pow(AproxDistOS(), 2); // d_dxa
      derivatives[1] = -DifYOS () / Math.Pow(AproxDistOS(), 2); // d_dya
      derivatives[2] = DifXOS ()  / Math.Pow(AproxDistOS(), 2); // d_dxb
      derivatives[3] = DifYOS ()  / Math.Pow(AproxDistOS(), 2); // d_dyb
      
      return derivatives;
    }

    // derivadas em ordem as coordenadas
    // o retorno eh um vetor [d/dx_o, d/dy_o, d/dx_e, d/dy_e, d/dx_v, d/dy_v]
    public double[] Derivatives ()
    {
      double[] derivatives = new double[6];

      double[] derivativesAuxOO = DerivativesAuxOO ();
      double[] derivativesAuxOS = DerivativesAuxOS ();

      // origem
      derivatives[0] = - derivativesAuxOO[2];
      derivatives[1] = - derivativesAuxOO[3];

      // visado
      derivatives[4] = derivativesAuxOS[2];
      derivatives[5] = derivativesAuxOS[3];

      // estacao
      derivatives[2] = derivativesAuxOS[0] - derivativesAuxOO[0];
      derivatives[3] = derivativesAuxOS[1] - derivativesAuxOO[1];
      
      // origem,estacao,visado
      return derivatives;
    }

  }
}