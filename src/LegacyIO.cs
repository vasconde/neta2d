//  LegacyIO.cs - Carrega os dados a partir de ficheiros nos formatos antigos do
//                do NGA/LNEC
//
//  Copyright 2013 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic; // list

using System.IO;
using System.Text.RegularExpressions;

namespace NetA2D
{
  public class LegacyIO
  {
    public static void NetworkInputToData (string networkInputPath, Network n,
					   Obs o, ConstraintEq c)
    {
      StreamReader sr = new StreamReader(networkInputPath);

      string line; // guarda linha a linha do ficheiro
      int i = 0;
      // 0 - coordenadas; 1 - angulos; 2 - distancias; 3 - fixos
      int estado = 0;
      bool isnew = true; // tem de reeiniciar? ou seja criar um novo spin
      SpinDAng s = null;
      double desAng=0; // desvio padrao das observacoes angulares
      double mm=0, ppm=0; // parametros para determinar a incerteza das distancias
      string repete = ""; // utilizado nos pontos fixos
      while((line = sr.ReadLine()) != null)
	{
	  if(i == 0 || i == 1) // comentarios
	    ; // para ja n faz nada
	  else if (line.Length > 0) // se n eh uma linha vazia
	    {
	      if (line[0] != '#')
		{
		  switch(estado)
		    {
		      // coordenadas aproximadas
		    case 0:
		      string[] words0 = line.Split(new char[] { ' ' },
						   StringSplitOptions.RemoveEmptyEntries);
		      n.AddPoint(words0[0], 
				 Convert.ToDouble(words0[1]),
				 Convert.ToDouble(words0[2]));
		      break;

		      // angulos
		    case 1:
		      string[] words1 = line.Split(new char[] { ' ' },
						   StringSplitOptions.RemoveEmptyEntries);
		      if (isnew == true) //inicio de novo giro?
			{
			  s = new SpinDAng(n.PointByName(words1[0]), 
					   n.PointByName(words1[1]),
					   Math.Sqrt(2.0)*desAng);

			  o.AddSpinDAng(s);
			}


		      s.AddDAng (new DAng(n.PointByName(words1[0]),
					  n.PointByName(words1[1]),
					  n.PointByName(words1[2]),
					  Convert.ToDouble(words1[3])));
		      break;

		      // distancias
		    case 2:
		      string[] words2 = line.Split(new char[] { ' ' },
						   StringSplitOptions.RemoveEmptyEntries);
		      o.AddDDist (new DDist(n.PointByName(words2[1]),
					    n.PointByName(words2[2]),
					    Convert.ToDouble(words2[3]),
					    mm, ppm, false));
		      break;

		      // pontos fixos
		    case 3:
		      string[] words3 = line.Split(new char[] { ' ' },
						   StringSplitOptions.RemoveEmptyEntries);

		    

		      if (repete == words3[0]) // se repete entao eh Y
			{

			  c.AddDispKnown (new DispKnown(n.PointByName(words3[0]), 1, Convert.ToDouble(words3[4]), Convert.ToDouble(words3[3])));
			}
		      else
			{
			  c.AddDispKnown (new DispKnown(n.PointByName(words3[0]), 0, Convert.ToDouble(words3[4]), Convert.ToDouble(words3[3])));
			}

		      repete = words3[0];
		      break;
		    }

		  isnew = false; // nao se trata de um novo giro
		}
	      else if(line[0] == '#')
		{
		  string[] wordsType = line.Split(new char[] { ' ' },
						  StringSplitOptions.RemoveEmptyEntries);
		  switch(wordsType[0])
		    {
		    case "#ANG":
		      estado = 1;
		      //string[] wordsA = line.Split();
		      desAng = Convert.ToDouble(wordsType[1]);
		      break;
		    case "#DIST":
		      estado = 2;
		      mm = Convert.ToDouble(wordsType[1]);
		      ppm = Convert.ToDouble(wordsType[2]);;
		      break;
		    case "#FIXOS":
		      estado = 3;
		      break;
		    }
		  isnew = true; // trata-se de um novo giro
		}
	    }
	  i++;
	}

      sr.Close(); // fecha o ficheiro
    }

    public static void A2DInputToData (string coordenadasFilePath,
				       string equacoesFilePath,
				       string difObsFilePath, 
				       Network n, Obs o, ConstraintEq c)
    {
      StreamReader srCoo = new StreamReader(coordenadasFilePath);
      StreamReader srEq = new StreamReader(equacoesFilePath);
      StreamReader srDifObs = new StreamReader(difObsFilePath);

      string line;
      double compensador; // ainda n serve para nada

      // coordenads
      // para a primeira linha
      line = srCoo.ReadLine();
      string[] words = line.Split(new char[] { ' ' },
				  StringSplitOptions.RemoveEmptyEntries);
      int numero = Convert.ToInt32(words[0]); // numero de coordenadas

      // para as restantes
      for(int i=0; (line = srCoo.ReadLine()) != null && i < numero; i++)
	{
	  words = line.Split(new char[] { ' ' },
	   		     StringSplitOptions.RemoveEmptyEntries);
	  n.AddPoint(words[0], 
		     Convert.ToDouble(words[1]),
		     Convert.ToDouble(words[2]));
	}

      // equacoes
      // para a primeira linha
      line = srEq.ReadLine();
      words = line.Split(new char[] { ' ' },
			 StringSplitOptions.RemoveEmptyEntries);

      int nEqQuocientes = Convert.ToInt32(words[0]); // eq. quocientes
      int nEqAngulos = Convert.ToInt32(words[1]); // eq. angulos
      int nEqDistancias = Convert.ToInt32(words[2]); // eq. quocientes
      int nEqCondicao = Convert.ToInt32(words[3]); // eq. condicao
      int nGirosQuocientes = Convert.ToInt32(words[4]); // giros de coencientes
      int nGirosAngulos = Convert.ToInt32(words[5]); // giros de angulos

      List<string> listaLinhas = new List<string>();
      while((line = srEq.ReadLine()) != null)
	{
	  listaLinhas.Add(line);
	}
      
      // angulos
      List <string> eqStringList = new List <string>();
      int contador = 0;
      int contador_linhas = 0;
      int oque = 0;
      bool primeirotitulo = true;
      string titulo1 = "", titulo2 = "";
      foreach(string l in listaLinhas)
	{
	  switch(oque)
	    {
	    case 0:
	      words = l.Split(new char[] { ' ' },
			      StringSplitOptions.RemoveEmptyEntries);

	      foreach(string elemento in words)
		{
		  eqStringList.Add(elemento);
		}

	      contador += words.Length;
	      contador_linhas++;

	      if(contador >= (nEqQuocientes + nEqAngulos + nEqDistancias
			      + nEqCondicao) * 3)
		{
		  oque = 1;
		  contador = 0;
		}
	      break;

	    case 1:
	      compensador = Convert.ToDouble(listaLinhas[contador_linhas]);
	      contador_linhas++;
	      oque = 2;
	      break;

	    case 2:
	      words = l.Split(new char[] { ' ' },
			      StringSplitOptions.RemoveEmptyEntries);

	      foreach(string elemento in words)
		{
		  eqStringList.Add(elemento);
		}

	      contador += words.Length;
	      contador_linhas++;

	      if(contador >= (nGirosQuocientes + nGirosAngulos) * 2)
		{
		  // se nao houver distancias salta logo para as eq.condicao
		  // eh necessario pq se n ele vai ignorar uma linha no
		  // foreach
		  if (nEqDistancias > 0)
		    {
		      oque = 3;
		      contador = 0;
		    }
		  else
		    {
		      oque = 4;
		      contador = 0;
		    }
		}

	      break;

	    case 3: // parametros dos erros para as distancias
	      
	      if (nEqDistancias > 0) // so faz se existirem distancias
		// passou a ser desnecessario pq este controlo eh feito
		// no passo anterior
		{
		  words = l.Split(new char[] { ' ' },
				  StringSplitOptions.RemoveEmptyEntries);
		  
		  foreach(string elemento in words)
		    {
		      eqStringList.Add(elemento);
		    }

		  contador += words.Length;
		  contador_linhas++;

		  if(contador >= 2)
		    {
		      oque = 4;
		      contador = 0;
		    }
		}
	      else
		{
		  oque = 4;
		  contador = 0;
		}
	      
	      break;

	    case 4: // incerteza das equacoes de condicao
	      words = l.Split(new char[] { ' ' },
			      StringSplitOptions.RemoveEmptyEntries);

	      foreach(string elemento in words)
		{
		  eqStringList.Add(elemento);
		}

	      contador += words.Length;
	      contador_linhas++;

	      if(contador >= nEqCondicao)
		{
		  oque = 5;
		  contador = 0;
		  primeirotitulo = true;
		}

	      break;

	    case 5: // titulos
	      if (primeirotitulo)
		{
		  titulo1 = l;
		  primeirotitulo = false;
		}
	      else
		{
		  titulo2 = l;
		  oque = 6;
		}
	      contador_linhas++;
	      break;

	    }
	  
	  
	}

      // diferencas de observacoes

      // carrega as linhas para uma lista
      listaLinhas = new List<string>();
      while((line = srDifObs.ReadLine()) != null)
	{
	  listaLinhas.Add(line);
	}

      // separa por elementos
      List <string> difObsStringList = new List <string>();
      contador = 0;

      foreach(string l in listaLinhas)
	{
	  words = l.Split(new char[] { ' ' },
			  StringSplitOptions.RemoveEmptyEntries);
	 
	  if (words.Length > 0) // marimba-se para as linhas em branco
	    {
	      foreach(string elemento in words)
		{
		  difObsStringList.Add(elemento);
		}
	    }
	}

      // foreach(string lll in eqStringList)
      // 	{
      // 	  Console.WriteLine(lll);
      // 	}
      // Console.WriteLine(titulo1);
      // Console.WriteLine(titulo2);

      // foreach(string lll in difObsStringList)
      // 	{
      // 	  Console.WriteLine(lll);
      // 	}


      // PRENCHER AS ESTRUTURAS DE DADOS
      int k, m;

      // giros dos quocientes
      int oo;
      for(oo = 3*(nEqQuocientes+nEqAngulos+nEqDistancias+nEqCondicao);
	  oo < 3*(nEqQuocientes+nEqAngulos+nEqDistancias+nEqCondicao)
	    +(nGirosQuocientes*2);
	  oo+=2)
	;

      // quocientes
      for(k = 0, m = 0; k < nEqQuocientes; k++, m+=3)
	;

      List<Tuple<double, int>> gangulos = new List<Tuple<double, int>>();
      for( ; oo < 3*(nEqQuocientes+nEqAngulos+nEqDistancias+nEqCondicao)
	     +(nGirosQuocientes*2)+(nGirosAngulos*2);
	   oo+=2)
	{
	  gangulos.Add(new Tuple<double,int> (Convert.ToDouble(eqStringList[oo]), Convert.ToInt32(eqStringList[oo+1])));
	}
      
      // angulos
      bool novogiro = true; // cria um novo giro?
      int obsPorGiro = 0;   // numero de visadas por giro
      int nGiro = 0;        // indice de giro
      SpinDAng s = null;
      for( ; k < nEqQuocientes+nEqAngulos; k++, m+=3)
	{
	  if (novogiro)
	    {
	      obsPorGiro = gangulos[nGiro].Item2;

	      Point p11 = A2DPointByIndex(Convert.ToInt32(eqStringList[m]),n);
	      Point p22 = A2DPointByIndex(Convert.ToInt32(eqStringList[m+1]),n);

	      s = new SpinDAng(p11, p22, Math.Sqrt(2.0)*gangulos[nGiro].Item1);
	      
	      o.AddSpinDAng(s);

	      novogiro = false; // para nao criar um giro na proxima passagem
	      nGiro++; // avanca no indice do giro
	    }

	  Point p1 = A2DPointByIndex(Convert.ToInt32(eqStringList[m]),n);
	  Point p2 = A2DPointByIndex(Convert.ToInt32(eqStringList[m+1]),n);
	  Point p3 = A2DPointByIndex(Convert.ToInt32(eqStringList[m+2]),n);

	  double diif = Convert.ToDouble(difObsStringList[k]);

	  s.AddDAng (new DAng(p1, p2, p3, diif));

	  obsPorGiro--;
	  if(obsPorGiro == 0) // nao ha mais observacoes para este giro
	    novogiro = true;  // sera criado um novo giro na proxima passagem
	}

      k++; // coeficiente do tal codigo

      // distancias

      if (nEqDistancias > 0)
	{
	  // aqui
	  double a = Convert.ToDouble(eqStringList[oo]);
	  oo++;
	  double b = Convert.ToDouble(eqStringList[oo]);
	  oo++;
      
	  for( ; k < nEqQuocientes+nEqAngulos+1+nEqDistancias; k++, m+=3)
	    {

	      Point p1 = A2DPointByIndex(Convert.ToInt32(eqStringList[m+1]),n);
	      Point p2 = A2DPointByIndex(Convert.ToInt32(eqStringList[m+2]),n);
	  
	      double diif = Convert.ToDouble(difObsStringList[k]);

	      o.AddDDist (new DDist(p1, p2, diif, a, b, false));
	  
	    }
	}

      // equacoes de condicao
      List< Tuple<Point, double, double> > listaFixos =
	new List< Tuple<Point, double, double> >();

      for( ; k < nEqQuocientes+nEqAngulos+1+nEqDistancias+nEqCondicao;
	   k++, m+=3)
	{
	  Point p1 = A2DPointByIndex(Convert.ToInt32(eqStringList[m+2]),n);
	  double diif = Convert.ToDouble(difObsStringList[k]);

	  listaFixos.Add(new Tuple<Point, double, double>(p1,diif,Convert.ToDouble(eqStringList[oo])));
	  oo++;
	  // n.Unknown2ControlByName(p1.Name);
	  
	  // if (repete == p1.Name) // se repete entao eh Y
	  //   {
	  //     p1.ControlY = true;
	  //     p1.DYA = diif;
	  //     p1.StdDYA = 1.1;
	  //     //Console.WriteLine(n.PointByName(words3[0]).ControlY);
	  //   }
	  // else
	  //   {
	  //     p1.ControlX = true;
	  //     p1.DXA = diif;
	  //     p1.StdDXA = 1.1;
	  //   }

	  // repete = p1.Name;

	}


      // List<double> desEqCondicao = new List<double>();
      // for(int it=0; it < nEqCondicao; it++)
      // 	{
      // 	  desEqCondicao.Add(Convert.ToDouble(eqStringList[oo]));
      // 	  oo++;
      // 	}

      string repete = "";
      DispKnown dkAux = null;
      foreach(Tuple<Point, double, double> ttt in listaFixos)
	{
	  if (repete == ttt.Item1.Name) // se repete entao eh Y
	    {
	      dkAux = new DispKnown(ttt.Item1, 1, ttt.Item2, ttt.Item3);
	      c.AddDispKnown (dkAux);
	    }
	  else
	    {
	      dkAux = new DispKnown(ttt.Item1, 0, ttt.Item2, ttt.Item3);
	      c.AddDispKnown (dkAux);
	    }

	  repete = ttt.Item1.Name;
	}

      srCoo.Close();
      srEq.Close();
      srDifObs.Close();
    }

    // funcao auxiliar para utilzar na A2DInputToData
    static Point A2DPointByIndex (int index, Network n)
    {
      int i = 1;
      foreach( Point p in n.Points )
	{
	  if(i == index)
	    return p;
	  i++;
	}
      return null;
    }
  }
}