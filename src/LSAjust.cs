//  LSAjust.cs - Ajustamento pelo metodo dos minimos quadrados
//
//  Copyright 2013, 2014, 2015 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;

namespace NetA2D
{
  public class LSAjust
  {
    LSMatrices m;

    public LSMatrices M
    {
      get { return m; }
      set { m = value; }
    }

    // construtor
    
    public LSAjust(LSMatrices m)
    {
      this.m = m;
    }

    /// <summary>
    ///   Ajusta as observacoes obtendo-se os seus residuos
    ///   e os deslocamentos estimados
    /// </summary>
    public void ComputeAjust ()
    {
      // calculo dos deslocamentos

      Matrix N = m.A.Transpose() * m.W * m.A;

      m.DX = N.Inverse * m.A.Transpose() * m.W * m.DY;

      // calculos dos residuos

      m.V = m.A * m.DX - m.DY;

      ComputeResidualStatistics ();
      ComputeOtherStatistics ();

      // sigma
      // Matrix sM = m.V.Transpose() * m.W * m.V;

      // m.S0 = Math.Sqrt(sM[0,0] / m.Df);
      
    }

    /// <summary>
    ///   Calcula as estatisticas associadas aos residuos das observacoes
    ///   Isto eh: vtWv; df; so; vtWvDAng; dfDAng ...
    /// </summary>
    public void ComputeResidualStatistics ()
    {

      // CALCULO DA MATRIZ I-U

      Matrix N = m.A.Transpose() * m.W * m.A;

      Matrix I = new Matrix (m.A.Rows, m.W.Columns, 1.0);
      
      Matrix IminusU = I - (m.A * N.Inverse * m.A.Transpose() * m.W);

      double sum = 0; // matriz auxiliar
      Matrix subIminusU = null; // matriz auxiliar

      // TUDO
      // soma do traco da matriz I-U // ATENCAO ATENCAO
      sum = 0;
      for(int i = 0; i < IminusU.Rows; i++)
	for(int j = 0; j < IminusU.Rows; j++)
	  if (i == j)
	    {
	      sum += IminusU[i,j];
	    }
      m.Df = sum;
      
      Matrix sM = m.V.Transpose() * m.W * m.V;
      m.VtWv = sM[0,0];

      m.S0 = Math.Sqrt(m.VtWv / m.Df);


      // CALCULOS DO S0 PARA AS OBSERVACOES GEODESICAS //

      // ANGULOS E DISTANCIAS

      // gera as submatriz de W e V que so contemplam as observacoes
      // geodesicas retirando assim a parte relativa as equacoes de
      // condicao
      Matrix subV = m.V.Submatrix(0, (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1,
				  0, 0);

      Matrix subW = m.W.Submatrix(0, (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1,
				  0, (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1);

      // double subDf = m.NDAngs + m.NDDists + m.NDLnDists - m.NUnknowns;

      subIminusU = IminusU.Submatrix(0, (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1,
				     0, (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1);

      // soma do traco da matriz I-U // ATENCAO ATENCAO
      sum = 0;
      for(int i = 0; i < subIminusU.Rows; i++)
	for(int j = 0; j < subIminusU.Rows; j++)
	  if (i == j)
	    {
	      sum += subIminusU[i,j];
	    }
      m.DfObs = sum;
      
      sM = subV.Transpose() * subW * subV;
      m.VtWvObs = sM[0,0];

      m.S0Obs = Math.Sqrt(m.VtWv / m.Df);


      // ANGULOS //
      
      if (m.NDAngs > 0)
	{
	  subV = m.V.Submatrix(0, (m.NDAngs)-1, 0, 0);
	  subW = m.W.Submatrix(0, (m.NDAngs)-1,
			       0, (m.NDAngs)-1);

	  subIminusU = IminusU.Submatrix(0, (m.NDAngs)-1,
					 0, (m.NDAngs)-1);

	  // soma do traco da matriz I-U
	  sum = 0;
	  for(int i = 0; i < subIminusU.Rows; i++)
	    for(int j = 0; j < subIminusU.Rows; j++)
	      if (i == j)
		{
		  sum += subIminusU[i,j];
		}
	  m.DfDAng = sum;
      
	  sM = subV.Transpose() * subW * subV;
	  m.VtWvDAng = sM[0,0];

	  m.S0DAng = Math.Sqrt(m.VtWvDAng / m.DfDAng);
	}

      // DISTANCIAS //

      if (m.NDDists > 0)
	{
	  subV = m.V.Submatrix(m.NDAngs, (m.NDAngs + m.NDDists)-1, 0, 0);
	  subW = m.W.Submatrix(m.NDAngs, (m.NDAngs + m.NDDists)-1,
			       m.NDAngs, (m.NDAngs + m.NDDists)-1);
      
	  subIminusU = IminusU.Submatrix(m.NDAngs, (m.NDAngs + m.NDDists)-1,
					 m.NDAngs, (m.NDAngs + m.NDDists)-1);
      
	  // soma do traco da matriz I-U
	  sum = 0;
	  for(int i = 0; i < subIminusU.Rows; i++)
	    for(int j = 0; j < subIminusU.Rows; j++)
	      if (i == j)
		{
		  sum += subIminusU[i,j];
		}
	  m.DfDDist = sum;

	  sM = subV.Transpose() * subW * subV;
	  m.VtWvDDist = sM[0,0];

	  m.S0DDist = Math.Sqrt(m.VtWvDDist / m.DfDDist);

	}

      // LN DISTANCIAS //

      if (m.NDLnDists > 0)
	{
	  subV = m.V.Submatrix(m.NDAngs + m.NDDists, (m.NDAngs + m.NDDists + m.NDLnDists)-1, 0, 0);
	  subW = m.W.Submatrix(m.NDAngs + m.NDDists, (m.NDAngs + m.NDDists + m.NDLnDists)-1,
			       m.NDAngs + m.NDDists, (m.NDAngs + m.NDDists + m.NDLnDists)-1);
      
	  subIminusU = IminusU.Submatrix(m.NDAngs + m.NDDists, (m.NDAngs + m.NDDists + m.NDLnDists)-1,
					 m.NDAngs + m.NDDists, (m.NDAngs + m.NDDists + m.NDLnDists)-1);
      
	  // soma do traco da matriz I-U
	  sum = 0;
	  for(int i = 0; i < subIminusU.Rows; i++)
	    for(int j = 0; j < subIminusU.Rows; j++)
	      if (i == j)
		{
		  sum += subIminusU[i,j];
		}
	  m.DfDLnDist = sum;

	  sM = subV.Transpose() * subW * subV;
	  m.VtWvDLnDist = sM[0,0];

	  m.S0DLnDist = Math.Sqrt(m.VtWvDLnDist / m.DfDLnDist);

	}

      // QUOCIENTES

      if (m.NDQuos > 0)
	{
	  subV = m.V.Submatrix(m.NDAngs + m.NDDists + m.NDLnDists,
			       (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1, 0, 0);

	  subW = m.W.Submatrix(m.NDAngs + m.NDDists + m.NDLnDists,
			       (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1,
			       m.NDAngs + m.NDDists + m.NDLnDists,
			       (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1);
      
	  subIminusU = IminusU.Submatrix(m.NDAngs + m.NDDists + m.NDLnDists,
					 (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1,
					 m.NDAngs + m.NDDists + m.NDLnDists,
					 (m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos)-1);
      
	  // soma do traco da matriz I-U
	  sum = 0;
	  for(int i = 0; i < subIminusU.Rows; i++)
	    for(int j = 0; j < subIminusU.Rows; j++)
	      if (i == j)
		{
		  sum += subIminusU[i,j];
		}
	  m.DfDQuo = sum;

	  sM = subV.Transpose() * subW * subV;
	  m.VtWvDQuo = sM[0,0];

	  m.S0DQuo = Math.Sqrt(m.VtWvDQuo / m.DfDQuo);
	}


      // RESTO

      if (m.NDispKnowns + m.NSumPairs > 0)
	{

	  int nobs = m.NDAngs + m.NDDists + m.NDLnDists + m.NDQuos;
	  int neqc = m.NDispKnowns + m.NSumPairs;

	  subV = m.V.Submatrix(nobs, nobs + neqc - 1, 0, 0);

	  subW = m.W.Submatrix(nobs, nobs + neqc - 1,
			       nobs, nobs + neqc - 1);
      
	  subIminusU = IminusU.Submatrix(nobs, nobs + neqc - 1,
					 nobs, nobs + neqc - 1);
      
	  // soma do traco da matriz I-U
	  sum = 0;
	  for(int i = 0; i < subIminusU.Rows; i++)
	    for(int j = 0; j < subIminusU.Rows; j++)
	      if (i == j)
		{
		  sum += subIminusU[i,j];
		}
	  m.DfEqc = sum;

	  sM = subV.Transpose() * subW * subV;
	  m.VtWvEqc = sM[0,0];

	  m.S0Eqc = Math.Sqrt(m.VtWvEqc / m.DfEqc);
	}

    }

    public void ComputeOtherStatistics ()
    {
      Matrix N = m.A.Transpose() * m.W * m.A;

      // Somatorio do quadrado dos deslocamentos

      Matrix aux = m.DX.Transpose() * m.DX;

      m.DxtDx = aux[0,0];

      // Quadrado da norma N dos deslocamentos

      aux = m.DX.Transpose() * N * m.DX;

      m.DxtNDx = aux[0,0];
    }

  }
}