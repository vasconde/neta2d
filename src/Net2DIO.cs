//  Net2DIO.cs - Carrega os dados a partir de um ficheiro de texto
//
//  Copyright 2013, 2014 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic; // list

using System.IO;
using System.Text.RegularExpressions;

namespace NetA2D
{
  public class Net2DIO
  {
    public static void InputTxtToData (string inputTxtPath, Network n, Obs o,
				       ConstraintEq c, List<string> notas,
				       bool typeStdDev = false)
    {
      StreamReader sr = new StreamReader(inputTxtPath);
      string line;
      Regex spaceAndTab = new Regex ("^[ \t]+$");

      int codigo = -1;
      int vez = -1;

      SpinDAng s = null;
      SpinDQuo q = null;
      double desPadAux = 0;
      double mm = 0, ppm = 0;

      while((line = sr.ReadLine()) != null)
	{
	  if (line.Length > 0 && !spaceAndTab.IsMatch(line)) // se nao eh uma linha vazia e nao eh so uma linha preenchida por espacos e tabs
	    {
	      string[] words = line.Split(new char[] { ' ', '\t' },
					  StringSplitOptions.RemoveEmptyEntries);
	      if(words[0][0] != '#') // se nao eh comentario
		{
		  if(words[0][0] == '*')
		    {
		      vez = 1;
		      // existem duas hipoteses
		      // *23 ou * 23
		      if ( words[0].Length == 1 )
			codigo = Convert.ToInt32(words[1]);
		      else
			{
			  codigo = Convert.ToInt32(words[0].Remove(0,1));
			}
		    }

		  switch (codigo)
		    {
		    case 0: // comentarios
		      if (vez == 1) // linha do codigo
			{
			  vez++;
			}
		      else
			{
			  notas.Add(line);
			}
		      
		      break;
		    case 1: // coordenadas aproximadas
		      if (vez == 1) // linha do codigo
			{
			  vez++;
			}
		      else // adiciona um novo ponto
			{ 
			  if(words.Length < 5)
			    {
			      n.AddPoint(words[0],
					 Convert.ToDouble(words[1]),
					 Convert.ToDouble(words[2]));
			    }
			  else if (words.Length == 5)
			    {
			      n.AddPoint(words[0],
					 Convert.ToDouble(words[1]),
					 Convert.ToDouble(words[2]),
					 Convert.ToDouble(words[4]) * Math.PI / 200.0);
			    }

			  vez++;
			}
		      break;
		    case 2: // observacoes
		      if (vez == 1)
			{
			  vez++;
			}
		      break;
		    case 20: // angulos
		      if (vez == 1) // linha do codigo
			{
			  vez++;
			}
		      else if (vez == 2) // linha do desvio padrao
			{
			  desPadAux = Convert.ToDouble(words[0]);
			  vez++;
			}
		      else
			{

			  if (vez == 3)
			    {
			      if(typeStdDev) // entra o stdev de uma diferenca de direcao
				{
				  s = new SpinDAng(n.PointByName(words[0]), 
						   n.PointByName(words[1]),
						   desPadAux);
				}
			      else // entra o stdev de uma direcao
				{
				  s = new SpinDAng(n.PointByName(words[0]), 
						   n.PointByName(words[1]),
						   Math.Sqrt(2.0)*desPadAux);
				}

			      o.AddSpinDAng(s);
			    }

			  s.AddDAng (new DAng(n.PointByName(words[0]),
					      n.PointByName(words[1]),
					      n.PointByName(words[2]),
					      Convert.ToDouble(words[3])));

			  vez++;
			}
		      break;
		    case 21: // distancias
		      if (vez == 1)
			{
			  vez++;
			}
		      else if (vez == 2)
			{
			  if(typeStdDev) // se for despad de uma dif de dist
			    {
			      mm = Convert.ToDouble(words[0]);

			      if (words.Length > 1)
				ppm = Convert.ToDouble(words[1]);
			      else
				ppm = 0.0;
			    }
			  else // se for os param de uma dist
			    {
			      mm = Convert.ToDouble(words[0]);
			      ppm = Convert.ToDouble(words[1]);
			    }


			  vez++;
			}
		      else
			{
			  if(typeStdDev)  // se for despad de uma dif de dist
			    {
			      o.AddDDist (new DDist(n.PointByName(words[0]),
						    n.PointByName(words[1]),
						    Convert.ToDouble(words[2]),
						    mm, ppm, true));
			    }
			  else // se for os param de uma dist
			    {
			      o.AddDDist (new DDist(n.PointByName(words[0]),
						    n.PointByName(words[1]),
						    Convert.ToDouble(words[2]),
						    mm, ppm, false));
			    }

			  vez++;
			}
		      break;
		    case 22: // ln distancias
		      if (vez == 1)
			{
			  vez++;
			}
		      else if (vez == 2)
			{
			  if(typeStdDev) // se for despad de uma dif de dist
			    {
			      mm = Convert.ToDouble(words[0]);
			    }
			  else // se for os param de uma dist
			    {
			      mm = Convert.ToDouble(words[0]);
			      ppm = Convert.ToDouble(words[1]);
			    }


			  vez++;
			}
		      else
			{
			  if(typeStdDev)  // se for despad de uma dif de dist
			    {
			      o.AddDLnDist (new DLnDist(n.PointByName(words[0]),
							n.PointByName(words[1]),
							Convert.ToDouble(words[2]),
							mm));
			    }
			  else // se for os param de uma dist
			    {
			      o.AddDLnDist (new DLnDist(n.PointByName(words[0]),
							n.PointByName(words[1]),
							Convert.ToDouble(words[2]),
							mm, ppm));
			    }

			  vez++;
			}
		      break;
		    case 23: // angulos
		      if (vez == 1) // linha do codigo
			{
			  vez++;
			}
		      else if (vez == 2) // linha do desvio padrao
			{
			  desPadAux = Convert.ToDouble(words[0]);
			  vez++;
			}
		      else
			{

			  if (vez == 3)
			    {
			      if(typeStdDev) // entra o stdev de uma diferenca de direcao
				{
				  q = new SpinDQuo(n.PointByName(words[0]), 
						   n.PointByName(words[1]),
						   Convert.ToDouble(words[2]),
						   desPadAux);
				}
			      else // entra o stdev de uma direcao
				{
				  q = new SpinDQuo(n.PointByName(words[0]), 
						   n.PointByName(words[1]),
						   Convert.ToDouble(words[2]),
						   Math.Sqrt(2.0)*desPadAux);
				}

			      o.AddSpinDQuo(q);
			    }

			  if (vez > 3)
			    {
			      q.AddDQuo (new DQuo(q,
						  n.PointByName(words[1]),
						  Convert.ToDouble(words[2])));
			    }

			  vez++;
			}
		      break;
		    case 3:  // fixos (eq. condicao)
		      if (vez == 1)
			{
			  vez++;
			}
		      else
			{
			  DispKnown dkAux = null;

			  if (words[1] == "X" || words[1] == "x")
			    {
			      dkAux = new DispKnown(n.PointByName(words[0]),
						    0, 
						    Convert.ToDouble(words[2]),
						    Convert.ToDouble(words[3]));
			      c.AddDispKnown (dkAux);
			    }
			  else if (words[1] == "Y" || words[1] == "y")
			    {
			      dkAux = new DispKnown(n.PointByName(words[0]),
						    1, 
						    Convert.ToDouble(words[2]),
						    Convert.ToDouble(words[3]));
			      c.AddDispKnown (dkAux);
			    }

			  vez++;
			}
		      break;

		    case 4: // eq. do tipo (dxa + dxb = 0)
		      if (vez == 1)
			{
			  vez++;
			}
		      else
			{

			  // a

			  int compAux1 = -1;
			  int signAux1 = -1;

			  if(words[0] == "+")
			    signAux1 = 0;
			  else
			    signAux1 = 1;

			  if(words[1] == "x" || words[1] == "X")
			    compAux1 = 0;
			  else
			    compAux1 = 1;
			  
			  // b

			  int compAux2 = -1;
			  int signAux2 = -1;
			  
			  if(words[3] == "+")
			    signAux2 = 0;
			  else
			    signAux2 = 1;

			  if(words[4] == "x" || words[4] == "X")
			    compAux2 = 0;
			  else
			    compAux2 = 1;

			  
			  
			  SumPair sp = new SumPair(n.PointByName(words[2]),
						   compAux1,
						   signAux1,
						   n.PointByName(words[5]),
						   compAux2,
						   signAux2,
						   Convert.ToDouble(words[6]));
			  c.AddSumPair (sp);
			}
		      break;
		    }
		}

	    }
	}

      sr.Close();
    }
  }
}