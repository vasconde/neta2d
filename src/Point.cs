//  Point.cs - Vertice da rede
//
//  Copyright 2013 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;

namespace NetA2D
{

  public class Point
  {
    // nome do ponto
    string name;
    
    // coordenadas planimetricas aproximadas
    double x, y;

    // angulo entre o referencial do NGA com o NO
    bool thetaThere; // so efetua projeccao se este valor for verdadeiro
    double theta;    // RADIANOS

    // parametros da elipse de erro a priori
    bool haveErrorEllip;
    double[] errorEllipParam;

    // *** Elementos A POSTERIORI ***
    // apos ajustamento
    
    // deslocamentos estimados
    double dX, dY;
    
    // desvio padrao a posteriori (intervalo de confianca)
    double stdDXP, stdDYP;

    // GET & SET

    // nome do ponto
    public string Name
    {
      get { return name; }
      set { name = value; }
    }
     
    // coordenadas planimetricas aproximadas
    public double X
    {
      get { return x; }
      set { x = value; }
    }
    
    public double Y
    {
      get { return y; }
      set { y = value; }
    }

    // angulo entre o referencial do NGA com o NO
    public bool ThetaThere
    {
      get { return thetaThere; }
      set { thetaThere = value; }
    }

    public double Theta
    {
      get { return theta; }
      set { theta = value; }
    }

    ///   Parametros da elipse de erro a priori
    ///   probabilidade, eixo maior, eixo menor, azimut
    public bool HaveErrorEllip
    {
      get { return haveErrorEllip; }
      set { haveErrorEllip = value; }
    }

    public double[] ErrorEllipParam
    {
      get { return errorEllipParam; }
      set { errorEllipParam = value; }
    }

    public void SetErrorEllip (double prob, double seM, double sem, double azM)
    {
      haveErrorEllip = true;
      errorEllipParam[0] = prob;
      errorEllipParam[1] = seM;
      errorEllipParam[2] = sem;
      errorEllipParam[3] = azM;
    }

    // APOS AJUSTAMENTO
    // deslocamentos estimados
    public double DX
    {
      get { return dX; }
      set { dX = value; }
    }

    public double DY
    {
      get { return dY; }
      set { dY = value; }
    }

    // componente tagencial
    public double DT
    {
      get { return dX * Math.Cos(theta) - dY * Math.Sin(theta); }
    }

    // componente radial
    public double DR
    {
      get { return dX * Math.Sin(theta) + dY * Math.Cos(theta); }
    }
    
    // desvio padrao a posteriori (intervalo de confianca)
    public double StdDXP
    {
      get { return stdDXP; }
      set { stdDXP = value; }
    }

    public double StdDYP
    {
      get { return stdDYP; }
      set { stdDYP = value; }
    }

    // CONSTRUTOR

    // construtor para um ponto da rede (nao de controlo)
    public Point( string name, double x, double y )
    {
      this.name = name;
      this.x = x;
      this.y = y;
      
      // sem angulo para projeccao
      this.thetaThere = false;

      haveErrorEllip = false;
      errorEllipParam = new double[4];
    }

    public Point( string name, double x, double y, double theta )
    {
      this.name = name;
      this.x = x;
      this.y = y;
      
      // angulo para projeccao
      this.thetaThere = true;
      this.theta = theta;

      haveErrorEllip = false;
      errorEllipParam = new double[4];
    }

  }
}