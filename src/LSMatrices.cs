//  LSMatrices.cs
//
//  Copyright 2013, 2014, 2015 Vasco Conde.
//
//  This file is part of NetA2D.
//
//  NetA2D is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  NetA2D is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with NetA2D.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Mapack;
using System.Collections.Generic; // list

namespace NetA2D
{

  public class LSMatrices
  {

    Network n;

    Obs o;

    ConstraintEq c;

    // vetor de observacoes (DY)
    Matrix dY;

    // matriz de pesos (W) - MCSO
    Matrix w;

    // vetor de coordenadas aproximadas (X)
    Matrix x0;

    // matriz de configuracao (A) - MCPO
    Matrix a;

    // vetor de parametros (DX)
    Matrix dX;

    // vetor dos residuos
    Matrix v;

    // Matriz das componentes das elipses [npts * 3, 1]
    // cM1 cm1 az1 cM2 cm2 az2 ...
    Matrix errorElipApriori;

    // estatisticas dos residuos

    // tudo (observacoes + condicoes)
    double vtWv;
    double df;
    double s0;

    // angular
    double vtWvDAng;
    double dfDAng;
    double s0DAng;

    // linear
    double vtWvDDist;
    double dfDDist;
    double s0DDist;

    // ln linear
    double vtWvDLnDist;
    double dfDLnDist;
    double s0DLnDist;

    // ln quo
    double vtWvDQuo;
    double dfDQuo;
    double s0DQuo;

    // soh observacoes
    double vtWvObs;
    double dfObs;
    double s0Obs;

    // equacoes de condicao
    double vtWvEqc;
    double dfEqc;
    double s0Eqc;

    // outras estatisticas
    double dxtNDx;

    double dxtDx;

    // ATRIBUTOS

    // get and set

    /// <summary>
    ///   Vertices da rede
    /// </summary>
    public Network N
    {
      get { return n; }
      set { n = value; }
    }

    /// <summary>
    ///   Observacoes geodesicas
    /// </summary>
    public Obs O
    {
      get { return o; }
      set { o = value; }
    }

    /// <summary>
    ///   Equacoes de constrangimento
    /// </summary>
    public ConstraintEq C
    {
      get { return c; }
      set { c = value; }
    }

    // vetor de observacoes (DY)
    public Matrix DY
    {
      get { return dY; }
      set { dY = value; }
    }

    // matriz de pesos (W) - MCSO
    public Matrix W
    {
      get { return w; }
      set { w = value; }
    }

    // vetor de coordenadas aproximadas (X)
    public Matrix X0
    {
      get { return x0; }
      set { x0 = value; }
    }

    // matriz de configuracao (A) - MCPO
    public Matrix A
    {
      get { return a; }
      set { a = value; }
    }

    // vetor de parametros (DX)
    public Matrix DX
    {
      get { return dX; }
      set { dX = value; }
    }

    // vetor dos residuos
    public Matrix V
    {
      get { return v; }
      set { v = value; }
    }

    /// <summary>
    ///   matriz das componentes das elipses de erro
    /// </summary>
    public Matrix ErrorElipApriori
    {
      get { return errorElipApriori; }
      set { errorElipApriori = value; }
    }


    // tudo (observacoes + condicoes)
    public double VtWv
    {
      get { return vtWv; }
      set { vtWv = value; }
    }

    public double Df
    {
      get { return df; }
      set { df = value; }
    }

    public double S0
    {
      get { return s0; }
      set { s0 = value; }
    }


    // angular
    public double VtWvDAng
    {
      get { return vtWvDAng; }
      set { vtWvDAng = value; }
    }

    public double DfDAng
    {
      get { return dfDAng; }
      set { dfDAng = value; }
    }

    public double S0DAng
    {
      get { return s0DAng; }
      set { s0DAng = value; }
    }

    // linear
    public double VtWvDDist
    {
      get { return vtWvDDist; }
      set { vtWvDDist = value; }
    }

    public double DfDDist
    {
      get { return dfDDist; }
      set { dfDDist = value; }
    }

    public double S0DDist
    {
      get { return s0DDist; }
      set { s0DDist = value; }
    }

    // ln linear

    public double VtWvDLnDist
    {
      get { return vtWvDLnDist; }
      set { vtWvDLnDist = value; }
    }

    public double DfDLnDist
    {
      get { return dfDLnDist; }
      set { dfDLnDist = value; }
    }

    public double S0DLnDist
    {
      get { return s0DLnDist; }
      set { s0DLnDist = value; }
    }

    // Quo

    public double VtWvDQuo
    {
      get { return vtWvDQuo; }
      set { vtWvDQuo = value; }
    }

    public double DfDQuo
    {
      get { return dfDQuo; }
      set { dfDQuo = value; }
    }

    public double S0DQuo
    {
      get { return s0DQuo; }
      set { s0DQuo = value; }
    }


    //  soh observacoes
    public double VtWvObs
    {
      get { return vtWvObs; }
      set { vtWvObs = value; }
    }

    public double DfObs
    {
      get { return dfObs; }
      set { dfObs = value; }
    }

    public double S0Obs
    {
      get { return s0Obs; }
      set { s0Obs = value; }
    }

    // equacoes de condicao

    public double VtWvEqc
    {
      get { return vtWvEqc; }
      set { vtWvEqc = value; }
    }

    public double DfEqc
    {
      get { return dfEqc; }
      set { dfEqc = value; }
    }

    public double S0Eqc
    {
      get { return s0Eqc; }
      set { s0Eqc = value; }
    }


    /// <summary>
    ///   Quadrado da norma N do vetor de deslocamentos Dx' N Dx
    /// </summary>
    public double DxtNDx
    {
      get { return dxtNDx; }
      set { dxtNDx = value; }
    }

    /// <summary>
    ///   Somatorio do quadrados dos deslocamentos
    /// </summary>
    public double DxtDx
    {
      get { return dxtDx; }
      set { dxtDx = value; }
    }

    /// <summary>
    ///   Numero de diferencas de angulos
    /// </summary>
    public int NDAngs
    {
      get
	{
	  return o.NDAngs;
	}
    }
    
    /// <summary>
    ///   Numero de diferencas de distancias
    /// </summary>
    public int NDDists
    {
      get
	{
	  return o.NDDists;
	}
    }

    /// <summary>
    ///   Numero de diferencas de ln distancias
    /// </summary>
    public int NDLnDists
    {
      get
	{
	  return o.NDLnDists;
	}
    }

    /// <summary>
    ///   Numero de quocientes
    /// </summary>
    public int NDQuos
    {
      get
	{
	  return o.NDQuos;
	}
    }

    /// <summary>
    ///   Numero de equacoes de condicao (e.g. dX = 0)
    /// </summary>
    public int NDispKnowns
    {
      get
	{
	  return c.NDispKnowns;
	}
    }

    /// <summary>
    ///   Numero de equacoes de condicao (e.g. dX1+dX2=0)
    /// </summary>
    public int NSumPairs
    {
      get
	{
	  return c.NSumPairs;
	}
    }

    /// <summary>
    ///   Numero de icognitas
    /// </summary>
    public int NUnknowns
    {
      get
	{
	  return n.NPointsComponents;
	}
    }

    // // numero de graus de liberdade
    // public int Df
    // {
    //   get { return a.Rows - a.Columns; }
    // }

    // NAO EH USADO (INICIO)

    // retorna o sub vetor dos residuos correspondente as
    // diferencas de angulos
    public Matrix SubVDAng
    {
      get
	{
	  return v.Submatrix(0, o.NDAngs-1, 0, 0);
	}
    }
    
    // retorna a sub matriz dos pesos correspondente as
    // diferencas de angulos
    public Matrix SubWDAng
    {
      get
	{
	  return w.Submatrix(0, o.NDAngs-1, 0, o.NDAngs-1);
	}
    }

    // NAO EH USADO (FIM)

    // CONSTRUTOR

    public LSMatrices(Network n, Obs o, ConstraintEq c)
    {
      this.n = n;
      this.o = o;
      this.c = c;

      // gera as matrizes com base na rede e nas observacoes
      GMatrices ();
    }

    // FUNCOES

    // gera matrizes baseado na network e no obs
    public void GMatrices ()
    {
      GDY(); // gera matriz de observacoes
      GX0();  // gera matriz de coordenadas aproximadas
      GW(); // gera matriz de pesos das observacoes
      GA(); // gera a mtriz de configuracao de primeira ordem
    }

    // atualiza network e obs com base nas matrizes
    public void UpdateData ()
    {
      // network
      int i = 0;
      foreach(Point p in n.Points)
	{
	  p.DX = dX[i,0];
	  i++;
	  p.DY = dX[i,0];
	  i++;
	}

      // OBS

      // angulos
      i = 0;
      foreach(SpinDAng s in o.SpinDAngs)
	{
	  foreach(DAng dang in s.Spin)
	    {
	      dang.Residual = v[i,0];
	      //dang.StdObsA = s.StdObsA;
	      i++;
	    }
	}
      
      // distancias
      foreach(DDist ddist in o.DDists)
	{
	  ddist.Residual = v[i,0];
	  i++;
	}

      // ln distancias
      foreach(DLnDist dlndist in o.DLnDists)
	{
	  dlndist.Residual = v[i,0];
	  i++;
	}

      // quocientes
      foreach(SpinDQuo s in o.SpinDQuos)
	{
	  foreach(DQuo dquo in s.Spin)
	    {
	      dquo.Residual = v[i,0];
	      dquo.StdObsA = Math.Sqrt(1.0/w[i,i]);
	      i++;
	    }
	}

      // equacoes de condicao (deslocamentos conhecidos)
      foreach(DispKnown dk in c.DispKnowns)
	{
	  dk.Residual = v[i,0];
	  i++;
	}

      // equacoes de condicao (somas de pares igual a 0)
      foreach(SumPair sp in c.SumPairs)
	{
	  sp.Residual = v[i,0];
	  i++;
	}

      // global
      o.VtWv = vtWv;
      o.Df = df;
      o.S0 = s0;

      // soh observacoes
      o.VtWv = vtWv;
      o.Df = df;
      o.S0 = s0;

      // angular
      o.VtWvDAng = vtWvDAng;
      o.DfDAng = dfDAng;
      o.S0DAng = s0DAng;

      // linear
      o.VtWvDDist = vtWvDDist;
      o.DfDDist = dfDDist;
      o.S0DDist = s0DDist;

      // linear
      o.VtWvDLnDist = vtWvDLnDist;
      o.DfDLnDist = dfDLnDist;
      o.S0DLnDist = s0DLnDist;

      // quocientes
      o.VtWvDQuo = vtWvDQuo;
      o.DfDQuo = dfDQuo;
      o.S0DQuo = s0DQuo;

      // soh observacoes
      o.VtWvObs = vtWvObs;
      o.DfObs = dfObs;
      o.S0Obs = s0Obs;

      // equacoes de condicao
      c.VtWvEqc = vtWvEqc;
      c.DfEqc = dfEqc;
      c.S0Eqc = s0Eqc;

      // quadrado da norma N do vetor de deslocamentos
      n.DxtNDx = dxtNDx;

      // somatorio do quadrado dos deslocamentos
      n.DxtDx = dxtDx;

    }
    
    // gera matriz de observacoes
    public void GDY()
    {

      // dimensao do vetor
      int nc = o.NDAngs + o.NDDists + o.NDLnDists + o.NDQuos + c.NDispKnowns + c.NSumPairs;


      // alocacao de memoria
      dY = new Matrix(nc, 1);

      int i = 0;

      // angulos
      foreach(SpinDAng s in o.SpinDAngs)
	{
	  foreach(DAng dang in s.Spin)
	    {
	      dY[i,0] = dang.Obs;
	      i++;
	    }
	}
      
      // distancias
      foreach(DDist ddist in o.DDists)
	{
	  dY[i,0] = ddist.Obs;
	  i++;
	}

      // ln distancias
      foreach(DLnDist dlndist in o.DLnDists)
	{
	  dY[i,0] = dlndist.LnObs;
	  i++;
	}


      // quocientes
      foreach(SpinDQuo s in o.SpinDQuos)
	{
	  foreach(DQuo dquo in s.Spin)
	    {
	      dY[i,0] = dquo.ObsDLnQuo;
	      i++;
	    }
	}

      // equacoes de condicao (deslocamentos conhecidos)
       foreach(DispKnown dk in c.DispKnowns)
	{
	  dY[i,0] = dk.Displacement;
	  i++;
	}

       // equacoes de condicao (somas de pares igual a 0)
       foreach(SumPair sp in c.SumPairs)
	 {
	   dY[i,0] = sp.SumResult;
	   i++;
	 }



      // Console.WriteLine(dY);
    }

    // gera a matriz de coordenadas aproximadas
    // nao me parece que isto va servir para alguma coisa
    public void GX0()
    {
      // aloca a memoria
      x0 = new Matrix(n.NPointsComponents,1);

      // preenche a matrix
      int i = 0;
      foreach(Point p in n.Points)
	{
	  x0[i,0] = p.X;
	  i++;
	  x0[i,0] = p.Y;
	  i++;
	}

      // Console.WriteLine(x0);
    }

    public void GW ()
    {
      // dimensao
      int ncl = o.NDAngs + o.NDDists + o.NDLnDists + o.NDQuos + c.NDispKnowns + c.NSumPairs;

      // alocacao de memoria
      // ele eh preenchida com zeros
      w = new Matrix(ncl,ncl);

      // angulos
      int ig = 0;
      foreach(SpinDAng s in o.SpinDAngs)
	{
	  Matrix subW = s.WMatrix;
	  
    
	  for(int ig_aux = ig, i=0 ; i < s.NSpin; i++, ig_aux++)
	    {
	      for(int j=0, jg_aux = ig; j < s.NSpin; j++, jg_aux++)
		{
		  w[ig_aux,jg_aux] = subW[i,j];
		}
	    }
	  
	  ig = ig + s.NSpin;
	}
      
      // // distancias
      foreach(DDist ddist in o.DDists)
      	{
      	  w[ig,ig] = (1/Math.Pow(ddist.StdObsA,2));
      	  ig++;
      	}

      // // distancias
      foreach(DLnDist dlndist in o.DLnDists)
      	{
      	  w[ig,ig] = (1/Math.Pow(dlndist.StdObsA,2));
      	  ig++;
      	}

      // quocientes

      foreach(SpinDQuo s in o.SpinDQuos)
	{
	  Matrix subW = s.WMatrix;
	      
	  for(int ig_aux = ig, i=0 ; i < s.NSpin; i++, ig_aux++)
	    {
	      for(int j=0, jg_aux = ig; j < s.NSpin; j++, jg_aux++)
		{
		  w[ig_aux,jg_aux] = subW[i,j];
		}
	    }
	  
	  ig = ig + s.NSpin;
	}

      // equacoes de condicao (deslocamentos conhecidos)
       foreach(DispKnown dk in c.DispKnowns)
	{
	  w[ig,ig] = (1/Math.Pow(dk.StDev,2));
	  ig++;
	}

       // equacoes de condicao (somas de pares igual a 0)
       foreach(SumPair sp in c.SumPairs)
	 {
	   w[ig,ig] = (1/Math.Pow(sp.StdDev,2));
	   ig++;
	 }

      //Console.WriteLine(w);
    }

    // retorna o indice da camponente X (isX=true)
    // ou Y (isX=false) do ponto s
    // foi criada especialmente para a geracao da matriz A
    int indexX (Point s, bool isX)
    {
      int i = 0;
      foreach(Point p in n.Points)
	{
	  if (p == s)
	    {
	      if (isX)
		return i;
	      else
		return i+1;
	    }
	  i += 2;
	}
      return -1;
    }

    public void GA ()
    {
      // numero de linhas de colunas
      int nl = o.NDAngs + o.NDDists + o.NDLnDists + o.NDQuos 
	+ c.NDispKnowns + c.NSumPairs; // no final eq. condicao

      int nc = n.NPointsComponents;

      double constante = 2000000.0/Math.PI;  // ATENCAO; WARNING
      
      // gera a matriz vazia
      a = new Matrix(nl,nc);

      // iterador de linha
      int i = 0;

      // angulos
      foreach(SpinDAng s in o.SpinDAngs)
	{
	  foreach(DAng dang in s.Spin)
	    {
	      double[] der = dang.Derivatives ();

	      // origem
	      a[i, indexX (dang.Origin, true)] = der[0] * constante;
	      a[i, indexX (dang.Origin, false)] = der[1] * constante;

	      // ocupado
	      a[i, indexX (dang.Occupied, true)] = der[2] * constante;
	      a[i, indexX (dang.Occupied, false)] = der[3] * constante;

	      // visado
	      a[i, indexX (dang.Sighted, true)] = der[4] * constante;
	      a[i, indexX (dang.Sighted, false)] = der[5] * constante;

	      i++;
	    }
	}
      
      // distancias
      foreach(DDist ddist in o.DDists)
	{
	  double[] der = ddist.Derivatives ();

	  // ocupado
	  a[i, indexX (ddist.Occupied, true)] = der[0];
	  a[i, indexX (ddist.Occupied, false)] = der[1];

	  // visado
	  a[i, indexX (ddist.Sighted, true)] = der[2];
	  a[i, indexX (ddist.Sighted, false)] = der[3];

	  i++;
	}

      // ln distancias
      foreach(DLnDist dlndist in o.DLnDists)
	{
	  double[] der = dlndist.Derivatives ();

	  // ocupado
	  a[i, indexX (dlndist.Occupied, true)] = der[0] * constante;
	  a[i, indexX (dlndist.Occupied, false)] = der[1] * constante;

	  // visado
	  a[i, indexX (dlndist.Sighted, true)] = der[2] * constante;
	  a[i, indexX (dlndist.Sighted, false)] = der[3] * constante;

	  i++;
	}

      // quocientes
      foreach(SpinDQuo s in o.SpinDQuos)
	{
	  foreach(DQuo dquo in s.Spin)
	    {
	      double[] der = dquo.Derivatives ();

	      // origem
	      a[i, indexX (dquo.Origin, true)] = der[0] * constante;
	      a[i, indexX (dquo.Origin, false)] = der[1] * constante;

	      // ocupado
	      a[i, indexX (dquo.Occupied, true)] = der[2] * constante;
	      a[i, indexX (dquo.Occupied, false)] = der[3] * constante;

	      // visado
	      a[i, indexX (dquo.Sighted, true)] = der[4] * constante;
	      a[i, indexX (dquo.Sighted, false)] = der[5] * constante;

	      i++;
	    }
	}

      // equacoes de condicao (deslocamentos conhecidos)
      foreach(DispKnown dk in c.DispKnowns)
	{
	  if(dk.Componente == 0) // ou seja X
	    {
	      a[i, indexX (dk.P, true)] = 1.0;
	      i++;
	    }
	  if(dk.Componente == 1) // ou seja Y
	    {
	      a[i, indexX (dk.P, false)] = 1.0;
	      i++;
	    }
	}

      // equacoes de condicao (somas de pares igual a 0)
       foreach(SumPair sp in c.SumPairs)
	 {
	   // first
	   if (sp.CompFirst == 0) // ou seja X
	     {
	       if (sp.SignalFirst == 0) // ou seja +
		 {
		   a[i, indexX (sp.PointFirst, true)] = 1.0;
		 }
	       else // ou seja -
		 {
		   a[i, indexX (sp.PointFirst, true)] = -1.0;
		 }
	     }
	   else // ou seja Y
	     {
	       if (sp.SignalFirst == 0) // ou seja +
		 {
		   a[i, indexX (sp.PointFirst, false)] = 1.0;
		 }
	       else // ou seja -
		 {
		   a[i, indexX (sp.PointFirst, false)] = -1.0;
		 }
	     }

	   // b
	   if (sp.CompSecond == 0) // ou seja X
	     {
	       if (sp.SignalSecond == 0) // ou seja +
		 {
		   a[i, indexX (sp.PointSecond, true)] = 1.0;
		 }
	       else // ou seja -
		 {
		   a[i, indexX (sp.PointSecond, true)] = -1.0;
		 }
	     }
	   else // ou seja Y
	     {
	       if (sp.SignalSecond == 0) // ou seja +
		 {
		   a[i, indexX (sp.PointSecond, false)] = 1.0;
		 }
	       else // ou seja -
		 {
		   a[i, indexX (sp.PointSecond, false)] = -1.0;
		 }
	     }

	   i++; // avanca a linha
	 }

       //Console.WriteLine(a);
    }
    


  }

}